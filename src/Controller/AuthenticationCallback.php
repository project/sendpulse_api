<?php

namespace Drupal\sendpulse_api\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\sendpulse_api\Service\SendpulseApi;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Sendpulse Api Callback Controller.
 *
 * @package Drupal\sendpulse_api\Controller
 */
class AuthenticationCallback extends ControllerBase {
  /**
   * Config factory service.
   *
   * @var Drupal\Core\Config\ConfigFactory
   */
  protected $config;

  /**
   * Symfony\Component\HttpFoundation\RequestStack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * Drupal\Core\Messenger\MessengerInterface.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   *   Messenger Interface.
   */
  protected $messenger;

  /**
   * GuzzleHttp\Client.
   *
   * @var \GuzzleHttp\Client
   */
  protected $client;

  /**
   * Constructor function.
   *
   * @param \Drupal\Core\Config\ConfigFactory $config
   *   Config factory.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   Messenger interface.
   * @param Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   Request Stack.
   * @param GuzzleHttp\Client $client
   *   Guzzle HTTP client.
   * @param \Drupal\sendpulse_api\Service\SendpulseApi $sendpulseApi
   *   Sendpulse ems service.
   */
  public function __construct(ConfigFactory $config, MessengerInterface $messenger, RequestStack $request_stack, Client $client, SendpulseApi $sendpulseApi) {
    $this->config = $config;
    $this->messenger = $messenger;
    $this->client = $client;
    $this->requestStack = $request_stack;
    $this->sendpulseApi = $sendpulseApi;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('messenger'),
      $container->get('request_stack'),
      $container->get('http_client'),
      $container->get('sendpulse_api')
    );
  }

  /**
   * Callback URL handling for Sendpulse Api API.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   Request.
   *
   * @return array
   *   Return markup for the page.
   */
  public function callbackUrl(Request $request) {
    $code = $request->get('code');
    $error = $request->get('error');
    $errorDescription = $request->get('error_description');

    $settings = $this->sendpulseApi->getConfig();
    $api_secret = $settings['api_secret'] ?? NULL;
    $tokenUrl = $settings['token_url'] ?? NULL;
    $client_secret = $settings['client_secret'] ?? NULL;

    $hasRequiredFields = FALSE;

    $hasRequiredFields = $api_secret && $client_secret && $code;

    if ($hasRequiredFields === TRUE) {
      try {
        $client = $this->client;
        $formParams = [
          'code' => $code,
          'redirect_uri' => $this->requestStack->getCurrentRequest()->getSchemeAndHttpHost() . '/admin/config/services/sendpulse-api/callback',
          'grant_type' => 'authorization_code',
          'scope' => 'contact_data+campaign_data+offline_access',
        ];
        $headers = [];

        $headers = [
          'Authorization' => 'Basic ' . base64_encode($api_secret . ':' . $client_secret),
        ];

        $response = $client->request('POST', $tokenUrl, [
          'headers' => $headers,
          'form_params' => $formParams,
        ]);

        $json = json_decode($response->getBody()->getContents());

        if ($json && property_exists($json, 'access_token') && property_exists($json, 'refresh_token')) {
          $tokens = $this->config->getEditable('sendpulse_api.tokens');
          $tokens->clear('sendpulse_api.tokens');
          $tokens->set('access_token', $json->access_token);
          $tokens->set('refresh_token', $json->refresh_token);
          $tokens->set('timestamp', strtotime('now'));
          $tokens->save();

          $this->messenger->addMessage($this->t('Tokens were successfully saved.'));
        }
        else {

          $this->messenger->addMessage($this->t('There was a problem authorizing your account.'), $this->messenger::TYPE_ERROR);
        }
      }
      catch (RequestException $e) {
        watchdog_exception('sendpulse_api', $e);
        $this->messenger->addMessage($this->t('There was a problem authorizing your account.'), $this->messenger::TYPE_ERROR);
      }
      catch (\Exception $e) {
        watchdog_exception('sendpulse_api', $e);
        $this->messenger->addMessage($this->t('There was a problem authorizing your account.'), $this->messenger::TYPE_ERROR);
      }
    }
    else {
      $message = 'There was a problem authorizing your account. <br/>';

      if ($error) {
        $message .= 'Error: ' . $error . '<br/>';
      }

      if ($errorDescription) {
        $message .= '  Description: ' . $errorDescription;
      }

      $this->messenger->addMessage($this->t('@message', ['@message' => $message]), $this->messenger::TYPE_ERROR);

    }

    return new RedirectResponse(Url::fromRoute('sendpulse_api.config')->toString());
  }

}
