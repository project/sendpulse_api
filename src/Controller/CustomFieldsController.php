<?php

namespace Drupal\sendpulse_api\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\sendpulse_api\Service\SendpulseApi;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines CustomFieldsController class.
 */
class CustomFieldsController extends ControllerBase {

  /**
   * The Sendpulse API service.
   *
   * @var \Drupal\sendpulse_api\Service\SendpulseApi
   */
  protected SendpulseApi $sendpulseApi;
  
  /**
   * Constructor function.
   *
   * @param \Drupal\sendpulse_api\Service\SendpulseApi $sendpulseApi
   *   Sendpulse ems service.
   */
  public function __construct(SendpulseApi $sendpulseApi) {
    $this->sendpulseApi = $sendpulseApi;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('sendpulse_api')
    );
  }

  /**
   * Display the markup.
   *
   * @return array
   *   Return markup array.
   */
  public function content() {
    $fields = $this->sendpulseApi->getCustomFields(FALSE);
    $header = ['Custom Field Name', 'Field Type', 'Custom Field ID'];
    $rows = [];

    if ($fields && is_array($fields->custom_fields) && count($fields->custom_fields) > 0) {
      foreach ($fields->custom_fields as $field) {
        $rows[] = [
          $this->t('@label', ['@label' => $field->label]),
          $this->t('@type', ['@type' => $field->type]),
          [
            'data' => [
              '#markup' => '<code>' . $field->custom_field_id . '</code>',
            ],
          ],
        ];
      }

    }

    return [
      '#type' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      '#empty' => $this->t('There are no custom fields found.'),
    ];
  }

}
