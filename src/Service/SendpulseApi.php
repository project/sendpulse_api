<?php

namespace Drupal\sendpulse_api\Service;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Session\AccountProxy;
use Drupal\Core\Site\Settings;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\ClientException;
use Drupal\sendpulse_api\Plugin\ApiClient;

/**
 * Class SendpulseApi.
 *
 * Class to handle API calls to Sendpulse Api.
 */
class SendpulseApi {

  use StringTranslationTrait;

  /**
   * Drupal\Core\Cache\CacheBackendInterface.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   *   Drupal cache.
   */
  protected $cache;

  /**
   * Drupal\Core\Config\ConfigFactory.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   *   Drupal config.
   */
  protected $config;

  /**
   * Drupal\Core\Session\AccountProxy.
   *
   * @var \Drupal\Core\Session\AccountProxy
   *   Drupal current user.
   */
  protected $currentUser;

  /**
   * Drupal\Core\Logger\LoggerChannelFactoryInterface.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   *   Drupal logging.
   */
  protected $loggerFactory;

  /**
   * Drupal\Core\Messenger\MessengerInterface.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   *   Messenger Interface.
   */
  protected $messenger;

  /**
   * Drupal\Core\Site\Settings.
   *
   * @var \Drupal\Core\Site\Settings
   *   Drupal site settings.
   */
  protected $settings;

  /**
   * GuzzleHttp\Client.
   *
   * @var \GuzzleHttp\Client
   *   Guzzle HTTP Client.
   */
  protected $httpClient;

  /**
   * The module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The Sendpulse Api v3 API endpoint.
   *
   * @var string
   */
  protected $apiUrl = 'https://api.cc.email/v3';

  /**
   * The URL to use for authorization.
   *
   * @var string
   */
  protected $authUrl = 'https://authz.sendpulseapi.com/oauth2/default/v1/authorize';

  /**
   * The URL to use for token oauth.
   *
   * @var string
   */
  protected $tokenUrl = 'https://authz.sendpulseapi.com/oauth2/default/v1/token';

  /**
   * Constructs the class.
   *
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache
   *   The interface for cache implementations.
   * @param \Drupal\Core\Config\ConfigFactory $config
   *   The configuration object factory.
   * @param \Drupal\Core\Session\AccountProxyInterface $currentUser
   *   The current user account proxy service, which represents
   *   the logged-in user.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $loggerFactory
   *   The factory for logging channels.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The runtime messages sent out to individual users on the page.
   * @param \Drupal\Core\Site\Settings $settings
   *   The read settings that are initialized with the class.
   * @param \GuzzleHttp\Client $httpClient
   *   The client for sending HTTP requests.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   The module handler service.
   */
  public function __construct(CacheBackendInterface $cache, ConfigFactory $config, AccountProxy $currentUser, LoggerChannelFactoryInterface $loggerFactory, MessengerInterface $messenger, Settings $settings, Client $httpClient, ModuleHandlerInterface $moduleHandler) {
    $this->cache = $cache;
    $this->config = $config;
    $this->currentUser = $currentUser;
    $this->loggerFactory = $loggerFactory;
    $this->messenger = $messenger;
    $this->settings = $settings;
    $this->httpClient = $httpClient;
    $this->moduleHandler = $moduleHandler;
  }

  /**
   * Returns the configurations for the class.
   *
   * @return array
   *   Returns an array with all configuration settings.
   */
  public function getConfig() {
    // Get our settings from settings.php.
    $settings = $this->settings::get('sendpulse_api');
    $api_user_id = $settings['api_user_id'] ?? NULL;
    $api_secret = $settings['api_secret'] ?? NULL;
    $configType = 'settings.php';

    // If nothing is in settings.php, let's check our config files.
    if (!$settings) {
      $api_user_id = $this->config->get('sendpulse_api.config')->get('api_user_id');
      $api_secret = $this->config->get('sendpulse_api.config')->get('api_secret');
      $configType = 'config';
    }

    if (!$this->moduleHandler->moduleExists('automated_cron') && !$this->moduleHandler->moduleExists('ultimate_cron') && (int) $this->currentUser->id() !== 0 && $this->currentUser->hasPermission('administer sendpulse api configuration')) {
      $this->messenger->addMessage($this->t('It is recommended to install automated_cron or make sure that cron is run regularly to refresh access tokens from Sendpulse Api API.'), 'warning');
    }

    return [
      'api_user_id' => $api_user_id,
      'api_secret' => $api_secret,
      'config_type' => $configType,
      // Application api_secret and other info found in settings.php
      // or via config.
      'refresh_token' => $this->config->get('sendpulse_api.tokens')->get('refresh_token'),
      'authentication_url' => $this->authUrl,
      'token_url' => $this->tokenUrl,
      'contact_url' => $this->apiUrl . '/contacts',
      'contact_lists_url' => $this->apiUrl . '/contact_lists',
      'campaigns_url' => $this->apiUrl . '/emails',
      'campaign_activity_url' => $this->apiUrl . '/emails/activities',

      // Add fields to configuration for signup form block configuration.
      // @see https://sendpulse.com/integrations/api
      'fields' => [
        'name' => 'Name',
        'mobile' => 'Mobile',
      ],
    ];
  }

  /**
   * Encodes a given string to Base64 URL encoding.
   *
   * This method takes a string, encodes it in Base64, removes any
   * padding characters ('='),
   * and then replaces the standard Base64 characters ('+' and '/')
   * with URL-safe characters ('-' and '_').
   *
   * @param string $string
   *   The input string to encode.
   *
   * @return string
   *   The Base64 URL-encoded string.
   */
  public function base64UrlEncode($string) {
    $base64 = base64_encode($string);
    $base64 = trim($base64, '=');
    $base64url = strtr($base64, '+/', '-_');

    return $base64url;
  }

  /**
   * Generates a random PKCE Code Verifier.
   *
   * This method generates a cryptographically secure random string,
   * encodes it as a Base64 URL-safe string,
   * and returns it as the PKCE code verifier. The code verifier is
   * used in OAuth 2.0 flows to enhance security.
   *
   * @return string
   *   A URL-safe Base64-encoded PKCE code verifier string.
   */
  public function generateCodeVerifier() {
    $random = bin2hex(openssl_random_pseudo_bytes(32));
    $verifier = $this->base64UrlEncode(pack('H*', $random));

    return $verifier;
  }

  /**
   * Return the encoded code challenge.
   *
   * For the PKCE Code Verifier to send to the API Authorization Endpoint.
   *
   * Https://sendpulse.com/integrations/api.
   *
   * @param string $codeVerifier
   *   The PKCE code verifier that was generated and needs to be
   *   hashed and encoded
   *   into a code challenge to send to the API Authorization Endpoint.
   *
   * @return string
   *   A URL-safe Base64-encoded code challenge derived from the
   *   provided code verifier.
   */
  public function getCodeChallenge($codeVerifier) {
    return $this->base64UrlEncode(pack('H*', hash('sha256', $codeVerifier)));
  }

  /**
   * Builds the response body by populating it with data.
   *
   * This method processes the provided data array and populates
   * the given body object with the fields specified in the configuration.
   * It also processes any custom fields and adds them to the body.
   * The body is then returned after the data is set.
   *
   * @param array $data
   *   An associative array of data to populate the body. The keys
   *   should match the fields defined in the configuration.
   * @param object $body
   *   The body object to populate with data. This object will have
   *   its properties set based on the contents of the `$data` array.
   *
   * @return object
   *   The populated body object with the appropriate fields set.
   */
  protected function buildResponseBody(array $data, object $body) {

    $fields = $this->getConfig()['fields'];

    foreach ($fields as $field => $name) {
      if (isset($data[$field]) && $data[$field]) {
        $body->{$field} = $data[$field];
      }
    }

    if (isset($data['custom_fields']) && count($data['custom_fields']) > 0) {
      foreach ($data['custom_fields'] as $id => $value) {
        $body->custom_fields[] = ['custom_field_id' => $id, 'value' => $value];
      }
    }

    return $body;
  }

  /**
   * Creates a new contact by posting to Sendpulse Api API.
   *
   * @param array $data
   *   Array of data to send to Sendpulse Api.
   *    Requires 'email_address' key.
   *    Can also accept 'name' and 'mobile'.
   * @param array $listIDs
   *   An array of list UUIDs where we want to add this contact.
   *
   * @see https://sendpulse.com/integrations/api#!/Contacts/createContact
   */
  private function createContact(array $data, $listIDs) {
    $config = $this->getConfig();

    $body = (object) [
      'email_address' => (object) [
        'address' => NULL,
        'permission_to_send' => NULL,
      ],
      'name' => NULL,
      'mobile' => NULL,
      'create_source' => NULL,
      'list_memberships' => NULL,
    ];

    $body = $this->buildResponseBody($data, $body);

    // Add our required fields.
    $body->email_address->address = $data['email_address'];
    $body->email_address->permission_to_send = 'implicit';
    $body->list_memberships = $listIDs;
    $body->create_source = 'Account';

    $this->moduleHandler->invokeAll('sendpulse_api_contact_data_alter', [$data, &$body]);
    $this->moduleHandler->invokeAll('sendpulse_api_contact_create_data_alter', [$data, &$body]);

    try {
      $response = $this->httpClient->request('POST', $config['contact_url'], [
        'headers' => [
          'Authorization' => 'Bearer ' . $config['api_secret'],
          'cache-control' => 'no-cache',
          'content-type' => 'application/json',
          'accept' => 'application/json',
        ],
        'body' => json_encode($body),
      ]);

      $this->handleResponse($response, 'createContact');
    }
    catch (RequestException $e) {
      // Return the error to show an error on form submission.
      return $this->handleRequestException($e);
    }
    catch (ClientException $e) {
      $this->handleRequestException($e);
    }
    catch (\Exception $e) {
      $this->loggerFactory->get('sendpulse_api')->error($e);

      // Return the error to show an error on form submission.
      return ['error' => $e];
    }
  }

  /**
   * Fetch the details of a single campaign.
   *
   * @param string $id
   *   The id of the campaign.
   *
   * @return mixed
   *   An stdClass of the campaign.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   *
   * @see https://sendpulse.com/integrations/api
   */
  public function getCampaign(string $id) {
    $config = $this->getConfig();
    try {
      $response = $this->httpClient->request('GET', $config['campaigns_url'] . '/' . $id, [
        'headers' => [
          'Authorization' => 'Bearer ' . $config['api_secret'],
          'cache-control' => 'no-cache',
          'content-type' => 'application/json',
          'accept' => 'application/json',
        ],
      ]);

      $this->updateTokenExpiration();
      $json = json_decode($response->getBody()->getContents());
      return $json;
    }
    catch (RequestException $e) {
      $this->handleRequestException($e);
    }
    catch (ClientException $e) {
      $this->handleRequestException($e);
    }
    catch (\Exception $e) {
      $this->loggerFactory->get('sendpulse_api')->error($e);
    }
  }

  /**
   * Get the campaign activity details by id.
   *
   * @param string $id
   *   The id of the activity.
   *
   * @return mixed
   *   A stdClass of the activity.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   *
   * @see https://sendpulse.com/integrations/api
   */
  public function getCampaignActivity(string $id) {
    $config = $this->getConfig();
    $url = $config['campaign_activity_url'] . '/' . $id;
    $url .= '?include=permalink_url';
    try {
      $response = $this->httpClient->request('GET', $url, [
        'headers' => [
          'Authorization' => 'Bearer ' . $config['api_secret'],
          'cache-control' => 'no-cache',
          'content-type' => 'application/json',
          'accept' => 'application/json',
        ],
      ]);

      $this->updateTokenExpiration();
      $json = json_decode($response->getBody()->getContents());
      return $json;
    }
    catch (RequestException $e) {
      $this->handleRequestException($e);
    }
    catch (ClientException $e) {
      $this->handleRequestException($e);
    }
    catch (\Exception $e) {
      $this->loggerFactory->get('sendpulse_api')->error($e);
    }
  }

  /**
   * Returns a list of the campaigns.
   *
   * @param array $status
   *   An option to filter campaigns by status.
   *
   * @return array
   *   An array of campaigns.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   *
   * @see https://sendpulse.com/integrations/api
   */
  public function getCampaigns($status = []) {
    $config = $this->getConfig();
    try {
      $response = $this->httpClient->request('GET', $config['campaigns_url'], [
        'headers' => [
          'Authorization' => 'Bearer ' . $config['api_secret'],
          'cache-control' => 'no-cache',
          'content-type' => 'application/json',
          'accept' => 'application/json',
        ],
      ]);

      $this->updateTokenExpiration();
      $json = json_decode($response->getBody()->getContents());
      $list = [];

      foreach ($json->campaigns as $campaign) {
        if (!empty($status) && in_array($campaign->current_status, $status)) {
          $list[] = $campaign;
        }
        elseif (empty($status)) {
          $list[] = $campaign;
        }

      }
      return $list;
    }
    catch (RequestException $e) {
      $this->handleRequestException($e);
    }
    catch (ClientException $e) {
      $this->handleRequestException($e);
    }
    catch (\Exception $e) {
      $this->loggerFactory->get('sendpulse_api')->error($e);
    }
  }

  /**
   * Checks if a contact exists already.
   *
   * @param array $data
   *   Array of data to send. 'email_address' key is required.
   *
   * @return array
   *   Returns a json response that determines if a contact
   *   already exists or was deleted from the list.
   *
   * @see https://sendpulse.com/integrations/api
   */
  private function getContact(array $data) {
    $config = $this->getConfig();

    try {
      $response = $this->httpClient->request('GET', $config['contact_url'] . '?email=' . $data['email_address'], [
        'headers' => [
          'Authorization' => 'Bearer ' . $config['api_secret'],
          'cache-control' => 'no-cache',
          'content-type' => 'application/json',
          'accept' => 'application/json',
        ],
      ]);

      $this->updateTokenExpiration();
      $json = json_decode($response->getBody()->getContents());

      if ($json->contacts) {
        return $json;
      }
      else {
        return $this->getDeleted($this->apiUrl . '/contacts?status=deleted&include_count=TRUE', $data['email_address']);
      }
    }
    catch (RequestException $e) {
      // Return the error to show an error on form submission.
      return $this->handleRequestException($e);
    }
    catch (ClientException $e) {
      $this->handleRequestException($e);
    }
    catch (\Exception $e) {
      $this->loggerFactory->get('sendpulse_api')->error($e);

      // Return the error to show an error on form submission.
      return ['error' => $e];
    }
  }

  /**
   * Retrieves the contact lists from the Sendpulse API.
   *
   * This method fetches the contact lists available to the account
   * via the Sendpulse API.
   * It can return either a cached response or a fresh one, depending
   * on the `$cached` parameter.
   * If set to `TRUE`, it may return a cached version, which could
   * potentially cause issues if refresh tokens have expired, as cron
   * jobs might call the cached version.
   *
   * @param bool $cached
   *   Whether to return a cached response (default is `TRUE`).
   *   If set to `FALSE`, a fresh response will be retrieved from the
   *   Sendpulse API.
   *
   * @return array
   *   An array of mailing lists that the account has access to.
   *   Each list contains details such as the list name, ID, and other
   *   relevant metadata.
   *
   * @see https://www.drupal.org/project/sendpulse_api/issues/3282088
   * @see https://sendpulse.com/integrations/api
   * @see https://sendpulse.com/integrations/api
   */
  public function getMailingLists($cached = TRUE) {
    $config = $this->getConfig();

    $cid = 'sendpulse_api.lists';
    $cache = ($cached === TRUE ? $this->cache->get($cid) : NULL);

    if ($cache && $cache->data && count($cache->data) > 0) {
      return $cache->data;
    }
    else {

      if (isset($config['api_secret'])) {
        $api_user_id = $config['api_user_id'];
        $api_secret = $config['api_secret'];
        $spApiClient = new ApiClient($api_user_id, $api_secret, NULL, $this->loggerFactory);
        try {
          $mailing_lists = $spApiClient->listAddressBooks();
        }
        catch (Exception $e) {

        }

        if (!empty($mailing_lists)) {
          foreach ($mailing_lists as $list) {
            if (isset($list->id)) {
              $list_data = [
                'list_id' => $list->id,
                'name' => $list->name,
              ];
              $lists[$list->id] = (object) $list_data;
            }
          }

          if (isset($lists)) {
            $this->saveContactLists($lists);
            return $lists;
          }
          return NULL;
        }
        else {
          $this->messenger->addMessage($this->t('There was a problem getting your available contact lists.'), 'error');
        }

        try {
          $response = $this->httpClient->request('GET', $config['contact_lists_url'], [
            'headers' => [
              'Authorization' => 'Bearer ' . $config['api_secret'],
              'cache-control' => 'no-cache',
              'content-type' => 'application/json',
              'accept' => 'application/json',
            ],
          ]);

          $this->updateTokenExpiration();
          $json = json_decode($response->getBody()->getContents());
          $lists = [];

          if ($json->lists) {
            foreach ($json->lists as $list) {
              $lists[$list->list_id] = $list;
            }

            $this->saveContactLists($lists);
            return $lists;
          }
          else {
            $this->messenger->addMessage($this->t('There was a problem getting your available contact lists.'), 'error');
          }
        }
        catch (RequestException $e) {
          $this->handleRequestException($e);
          $this->messenger->addMessage($this->t('There was a problem getting your available contact lists.'), 'error');
        }
        catch (ClientException $e) {
          $this->handleRequestException($e);
          $this->messenger->addMessage($this->t('There was a problem getting your available contact lists.'), 'error');
        }
        catch (\Exception $e) {
          $this->loggerFactory->get('sendpulse_api')->error($e);
          $this->messenger->addMessage($this->t('There was a problem getting your available contact lists.'), 'error');
        }
      }
      else {
        return [];
      }
    }
  }

  /**
   * Returns the custom fields available from the Sendpulse API.
   *
   * This method retrieves the custom fields associated with the account.
   * Depending on the `$cached` parameter, it will either return a cached
   * version or fetch fresh data from the Sendpulse API.
   *
   * @param bool $cached
   *   Whether to return a cached response (default is `TRUE`).
   *   If set to `FALSE`, a fresh response will be retrieved from the
   *   Sendpulse API.
   *
   * @return mixed
   *   A `stdClass` object containing the custom fields available to the
   *   account. The structure of the object depends on the data returned
   *   from the Sendpulse API.
   *
   * @see https://sendpulse.com/integrations/apiapi_guide/get_custom_fields.html
   */
  public function getCustomFields($cached = TRUE) {
    if (version_compare(PHP_VERSION, '8.0', '<')) {
      $this->messenger->addMessage($this->t('The Sendpulse module is not compatible with your current version of PHP and should be upgraded to latest version or greater than 8.0.'), 'error');
    }
    else {
      $config = $this->getConfig();
      $cid = 'sendpulse_api.custom_fields';
      $cache = ($cached === TRUE ? $this->cache->get($cid) : NULL);

      if ($cache && !is_null($cache) && $cache->data && property_exists($cache->data, 'custom_fields')) {
        return $cache->data;
      }
      else {
        if (isset($config['api_secret'])) {

          $custom_fields_data = new \stdClass();

          $name = new \stdClass();
          $name->custom_field_id = 'name';
          $name->label = 'Name';
          $name->type = 'textfield';

          $phone = new \stdClass();
          $phone->custom_field_id = 'phone';
          $phone->label = 'Phone';
          $phone->type = 'textfield';
          $custom_fields = [$name, $phone];

          $custom_fields_data->custom_fields = $custom_fields;

          try {

            $this->cache->set($cid, $custom_fields_data);

            return $custom_fields_data;
          }
          catch (RequestException $e) {
            $this->handleRequestException($e);
          }
          catch (ClientException $e) {
            $this->handleRequestException($e);
          }
          catch (\Exception $e) {
            $this->loggerFactory->get('sendpulse_api')->error($e);
          }
        }
      }
    }
  }

  /**
   * Checks if a contact is deleted from a list.
   *
   * This loops through all the deleted contacts of a
   * list and returns if there is a match to the email address.
   *
   * @param string $endpoint
   *   The endpoint to check. @see $this->getContact()
   * @param string $email
   *   The email address we're looking for.
   *
   * @return array
   *   Returns an array of a matched deleted contact.
   *
   * @see https://community.sendpulseapi.com/t5/Developer-Support-ask-questions/API-v-3-409-conflict-on-POST-create-a-Contact-User-doesn-t/td-p/327518
   */
  private function getDeleted($endpoint, $email) {
    $config = $this->getConfig();

    $deleted = $this->httpClient->request('GET', $endpoint, [
      'headers' => [
        'Authorization' => 'Bearer ' . $config['api_secret'],
        'cache-control' => 'no-cache',
        'content-type' => 'application/json',
        'accept' => 'application/json',
      ],
    ]);

    $deleted = json_decode($deleted->getBody()->getContents());
    $match = NULL;

    if (count($deleted->contacts)) {
      foreach ($deleted->contacts as $value) {
        if ($value->email_address->address === $email) {
          $match = $value;
        }
      }
    }

    if (!$match &&  property_exists($deleted, '_links') && property_exists($deleted->_links, 'next') && property_exists($deleted->_links->next, 'href')) {
      $match = $this->getDeleted('https://api.cc.email' . $deleted->_links->next->href, $email);
    }

    return $match;
  }

  /**
   * Retrieves the enabled contact lists.
   *
   * This method fetches the list of enabled contact lists. It calls the
   * `getMailingLists` method to retrieve the lists and can return either
   * a cached or fresh response based on the `$cached` parameter.
   *
   * @param bool $cached
   *   Whether to return a cached response (default is `TRUE`). If set to
   *   `FALSE`, a fresh response will be fetched from the Sendpulse API.
   *
   * @return array
   *   An array of enabled mailing lists available to the account. Each list
   *   in the array represents an enabled contact list.
   *
   * @see /Drupal/Form/SendpulseApiLists.php
   */
  public function getEnabledMailingLists($cached = TRUE) {
    $lists = $this->getMailingLists($cached);

    return $lists;
  }

  /**
   * Get the permanent link of a campaign.
   *
   * @param string $id
   *   The campaign id.
   *
   * @return null|string
   *   The URL of the campaign's permanent link.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function getPermaLinkFromCampaign(string $id) {
    if (!$id) {
      return NULL;
    }
    $campaign = $this->getCampain($id);
    foreach ($campaign->campaign_activities as $activity) {
      if ($activity->role != 'permalink') {
        continue;
      }
      $act = $this->getCampaignActivity($activity->campaign_activity_id);
      if ($act) {
        return $act->permalink_url;
      }
    }
    return NULL;
  }

  /**
   * Handles an error when an exception is thrown during a request.
   *
   * This method processes the given exception object, logs the error, and
   * returns an appropriate response array to be used further in the
   * application.
   *
   * @param object $e
   *   The exception object that contains the error details. It is typically
   *   an instance of an exception class (e.g., `RequestException`).
   *
   * @return array
   *   An array containing the error details or an appropriate error message
   *   that can be used for further handling or response processing.
   */
  protected function handleRequestException(object $e) {
    $response = $e->getResponse();
    $error = is_null($response) ? FALSE : json_decode($response->getBody());

    $message = 'RequestException: ';

    $errorInfo = [];

    if ($error && is_object($error)) {
      if (property_exists($error, 'error')) {
        $errorInfo[] = $error->error;
      }

      if (property_exists($error, 'message')) {
        $errorInfo[] = $error->message;
      }

      if (property_exists($error, 'error_description')) {
        $errorInfo[] = $error->error_description;
      }

      if (property_exists($error, 'errorCode')) {
        $errorInfo[] = $error->errorSummary;
      }

      if (property_exists($error, 'errorCode')) {
        $errorInfo[] = $error->errorCode;
      }
    }

    $message .= implode(', ', $errorInfo);

    $this->loggerFactory->get('sendpulse_api')->error($message);

    // Return the error to show an error on form submission.
    return ['error' => $message];
  }

  /**
   * Handles API response for adding a contact.
   *
   * @param object $response
   *   The json_decoded json response.
   * @param string $method
   *   The name of the method that the response came from.
   *
   * @return array
   *   Returns an array that includes the method name and
   *   the statuscode except if it is coming from getContact method.
   *   Then it returns an array of the contact that matches.
   */
  private function handleResponse($response, $method) {


    if ($response && method_exists($response, 'getError') && ($response->getError() != 'null' || ($response->getError() != 'null' && $method === 'createContact'))) {
      $statuscode = $response->getCode();
      $context = $response->getContext();

      $this->loggerFactory->get('sendpulse_api')->info('@method has been executed successfully.', ['@method' => $method]);

      $this->updateTokenExpiration();

      if ($method === 'getContact') {
        return $context;
      }

      return [
        'method' => $method,
        'response' => $statuscode,
      ];
    }
    else {
      if ($response) {
        $statuscode = (method_exists($response, 'getCode')) ? $response->getCode() : '';
        $responsecode = (method_exists($response, 'getError')) ? $response->getError() : '';

        if($statuscode && $responsecode) {
          $this->loggerFactory->get('sendpulse_api')->info('Call to @method resulted in @status response. @responsecode', [
            '@method' => $method,
            '@status' => $statuscode,
            '@responsecode' => $responsecode,
          ]);
        }
      }
    }
  }

  /**
   * Submits a contact to the API.
   *
   * Used to be used on CostantContactBlockForm but using
   * $this->submitContactForm instead.
   * Determine if contact needs to be updated or created.
   *
   * @param array $data
   *   Data to create/update a contact.
   *   Requires a 'email_address' key.
   *   But can also accept 'name' and 'mobile' key.
   * @param array $listIDs
   *   An array of list UUIDs to post the contact to.
   *
   * @return array
   *   Returns an error if there is an error.
   *   Otherwise it sends the info to other methods.
   *
   * @see $this->updateContact
   * @see $this->putContact
   * @see $this->createContact
   */
  public function postContact(array $data = [], $listIDs = []) {
    $config = $this->getConfig();
    $enabled = $this->config->get('sendpulse_api.enabled_lists')->getRawData();

    if (!$config['api_secret'] || !$config['client_secret'] || !$config['api_secret'] || !$data) {
      $msg = 'Missing credentials for postContact';

      $this->loggerFactory->get('sendpulse_api')->error($msg);

      return [
        'error' => $msg,
      ];
    }

    if (!$listIDs || count($listIDs) === 0) {
      $msg = 'A listID is required.';

      $this->loggerFactory->get('sendpulse_api')->error($msg);

      return [
        'error' => $msg,
      ];
    }

    foreach ($listIDs as $listID) {
      if (!isset($enabled[$listID]) || $enabled[$listID] !== 1) {
        $msg = 'The listID provided does not exist or is not enabled.';

        $this->loggerFactory->get('sendpulse_api')->error($msg);

        return [
          'error' => $msg,
        ];
      }
    }

    if (!isset($data['email_address'])) {
      $msg = 'An email address is required';

      $this->loggerFactory->get('sendpulse_api')->error($msg);

      return [
        'error' => $msg,
      ];
    }

    // Check if contact already exists.
    $exists = (array) $this->getContact($data);

    // If yes, updateContact.
    // If no, createContact.
    // If previous deleted, putContact.
    if (isset($exists['contacts']) && count($exists['contacts']) > 0) {
      $this->updateContact($data, $exists['contacts'][0], $listIDs);
    }
    elseif ($exists && isset($exists['deleted_at'])) {
      $this->putContact($exists, $data, $listIDs);
    }
    else {
      $this->createContact($data, $listIDs);
    }
  }

  /**
   * Updates a contact if it already exists and has been deleted.
   *
   * @param array $contact
   *   The response from $this->getDeleted.
   * @param array $data
   *   The $data provided originally. @see $this->postContact.
   * @param array $listIDs
   *   The list IDs we want to add contact to.
   *
   * @see https://sendpulse.com/integrations/api
   * @see $this->getDeleted
   *
   * @todo perhaps combine this with updateContact. The difference is that $contact is
   * an array here and an object in updateContact.
   */
  private function putContact(array $contact, array $data, $listIDs) {
    $config = $this->getConfig();

    $body = (object) $contact;

    $body = $this->buildResponseBody($data, $body);

    $body->email_address->permission_to_send = 'implicit';
    // To resubscribe a contact after an unsubscribe update_source must equal
    // Contact.
    // @see https://sendpulse.com/integrations/api
    $body->update_source = 'Contact';
    $body->list_memberships = $listIDs;

    $this->moduleHandler->invokeAll('sendpulse_api_contact_data_alter', [$data, &$body]);
    $this->moduleHandler->invokeAll('sendpulse_api_contact_update_data_alter', [$data, &$body]);

    try {
      $response = $this->httpClient->request('PUT', $config['contact_url'] . '/' . $contact['contact_id'], [
        'headers' => [
          'Authorization' => 'Bearer ' . $config['api_secret'],
          'cache-control' => 'no-cache',
          'content-type' => 'application/json',
          'accept' => 'application/json',
        ],
        'body' => json_encode($body),
      ]);

      $this->handleResponse($response, 'putContact');

    }
    catch (RequestException $e) {
      // Return the error to show an error on form submission.
      return $this->handleRequestException($e);
    }
    catch (ClientException $e) {
      return $this->handleRequestException($e);
    }
    catch (\Exception $e) {
      $this->loggerFactory->get('sendpulse_api')->error($e);

      // Return the error to show an error on form submission.
      return ['error' => $e];
    }
  }

  /**
   * Makes authenticated request to Sendpulse Api to refresh tokens.
   *
   * @see https://sendpulse.com/integrations/api
   */
  public function refreshToken($updateLists = TRUE) {
    $config = $this->getConfig();

    if (!$config['api_secret']) {
      return FALSE;
    }

    // @todo Fix for pkce flow.
    // @see https://www.drupal.org/project/sendpulse_api/issues/3285446
    // @see https://sendpulse.com/integrations/apiapi_guide/pkce_flow.html
    try {
      $response = $this->httpClient->request('POST', $this->tokenUrl, [
        'headers' => [
          'Authorization' => 'Basic ' . base64_encode($config['api_secret'] . ':' . $config['client_secret']),
        ],
        'form_params' => [
          'refresh_token' => $config['refresh_token'],
          'grant_type' => 'refresh_token',
        ],
      ]);

      $json = json_decode($response->getBody()->getContents());

      $this->saveTokens($json);

      if ($updateLists === TRUE) {
        $this->getMailingLists(FALSE);
      }
    }
    catch (RequestException $e) {
      return $this->handleRequestException($e);
    }
    catch (\Exception $e) {
      $this->loggerFactory->get('sendpulse_api')->error($e);

      // Return the error to show an error on form submission.
      return ['error' => $e];
    }
  }

  /**
   * Saves available contact lists to a cache.
   *
   * @param array $data
   *   An array of lists and list UUIDs from $this->getMailingLists.
   */
  public function saveContactLists(array $data) {
    $cid = 'sendpulse_api.lists';
    $enabled = $this->config->get('sendpulse_api.enabled_lists')->getRawData();

    // Add an enabled flag to the list data.
    foreach ($data as $key => $value) {
      $data[$key]->enabled = (isset($enabled[$key]) && $enabled[$key] === 1);
      $data[$key]->cached_on = strtotime('now');
    }

    $this->cache->set($cid, $data);
  }

  /**
   * Saves access and refresh tokens to our config database.
   *
   * @param object $data
   *   Data object of data to save the token.
   *
   * @see $this->refreshToken
   */
  private function saveTokens($data) {
    if ($data && property_exists($data, 'api_secret') && property_exists($data, 'refresh_token')) {
      $tokens = $this->config->getEditable('sendpulse_api.tokens');
      $tokens->clear('sendpulse_api.tokens');
      $tokens->set('api_secret', $data->api_secret);
      $tokens->set('refresh_token', $data->refresh_token);
      $tokens->set('timestamp', strtotime('now'));

      if ($data->expires_in < $tokens->get('expires')) {
        $tokens->set('expires', $data->expires_in);
      }

      $tokens->save();

      $this->loggerFactory->get('sendpulse_api')->info('New tokens saved at ' . date('Y-m-d h:ia', strtotime('now')));
    }
    else {
      $this->loggerFactory->get('sendpulse_api')->error('There was an error saving tokens');
    }
  }

  /**
   * Submission of contact form.
   *
   * Replaces $this->postContact as of v. 2.0.9.
   *
   * @param array $data
   *   Data to create/update a contact.
   *   Requires a 'email_address' key.
   *   But can also accept 'name' and 'mobile' key.
   * @param array $listIDs
   *   An array of list UUIDs to post the contact to.
   *
   * @return array
   *   Returns an error if there is an error.
   *   Otherwise it sends the info to other methods.
   *
   * @see https://sendpulse.com/integrations/api
   */
  public function submitContactForm(array $data = [], $listIDs = []) {
    $config = $this->getConfig();

    $enabled = $this->config->get('sendpulse_api.enabled_lists')->getRawData();

    if (!$config['api_secret'] || !$data) {
      $msg = 'Missing credentials for postContact';

      $this->loggerFactory->get('sendpulse_api')->error($msg);

      return [
        'error' => $msg,
      ];
    }

    if ($listIDs && !is_array($listIDs)) {
      $listIDs = [$listIDs];
    }

    if (!$listIDs || count($listIDs) === 0) {

      $msg = 'A listID is required.';

      $this->loggerFactory->get('sendpulse_api')->error($msg);

      return [
        'error' => $msg,
      ];
    }

    foreach ($listIDs as $listID) {
      if (!isset($enabled[$listID]) || $enabled[$listID] !== 1) {
        $msg = 'The listID provided does not exist or is not enabled.';

        $this->loggerFactory->get('sendpulse_api')->error($msg);

        return [
          'error' => $msg,
        ];
      }
    }

    if (!isset($data['email_address'])) {
      $msg = 'An email address is required';

      $this->loggerFactory->get('sendpulse_api')->error($msg);

      return [
        'error' => $msg,
      ];
    }

    $body = (object) [];

    $body = $this->buildResponseBody($data, $body);

    // Add required fields.
    $body->email_address = $data['email_address'];
    $body->list_memberships = $listIDs;

    $this->moduleHandler->invokeAll('sendpulse_api_contact_data_alter', [$data, &$body]);
    $this->moduleHandler->invokeAll('sendpulse_api_contact_form_submission_alter', [$data, &$body]);

    try {

      $api_user_id = $config['api_user_id'];
      $api_secret = $config['api_secret'];

      $email_address = $data['email_address'];
      $name = $data['name'] ?? '';
      $phone = $data['phone'] ?? '';

      $emails = [
      [
        'email' => $email_address,
        'variables' => [
          'phone' => $phone,
          'name' => $name,
        ],
      ],
      ];

      $spApiClient = new ApiClient($api_user_id, $api_secret, NULL, $this->loggerFactory);

      foreach ($listIDs as $bookID) {
        $response = $spApiClient->addEmails($bookID, $emails);

        $this->handleResponse($response, 'submitContactForm');

      }
    }
    catch (RequestException $e) {
      // Return the error to show an error on form submission.
      return $this->handleRequestException($e);
    }
    catch (ClientException $e) {
      $this->handleRequestException($e);
    }
    catch (\Exception $e) {
      $this->loggerFactory->get('sendpulse_api')->error($e);

      // Return the error to show an error on form submission.
      return ['error' => $e];
    }
  }

  /**
   * Unsubscribes a contact from all lists.
   *
   * @param array $data
   *   The $data provided.
   *
   * @see https://sendpulse.com/integrations/api
   */
  public function unsubscribeContact(array $data) {
    $config = $this->getConfig();
    $body = NULL;

    $email_address = $data['email_address'];
    $mailing_list_id = $data['mailing_list_id'] ?? '';

    $this->moduleHandler->invokeAll('sendpulse_api_contact_data_alter', [$data, &$body]);
    $this->moduleHandler->invokeAll('sendpulse_api_contact_unsubscribe_data_alter', [$data, &$body]);

    try {
      if ($mailing_list_id != '') {

        $api_user_id = $config['api_user_id'];
        $api_secret = $config['api_secret'];

        $emails = [
          $email_address,
        ];

        $spApiClient = new ApiClient($api_user_id, $api_secret, NULL, $this->loggerFactory);
        $response = $spApiClient->removeEmails($mailing_list_id, $emails);

        $this->handleResponse($response, 'unsubscribeContact');
      }
    }
    catch (RequestException $e) {
      // Return the error to show an error on form submission.
      return $this->handleRequestException($e);
    }
    catch (ClientException $e) {
      return $this->handleRequestException($e);
    }
    catch (\Exception $e) {
      $this->loggerFactory->get('sendpulse_api')->error($e);

      // Return the error to show an error on form submission.
      return ['error' => $e];
    }
    // }
  }

  /**
   * Updates a contact if it already exists on a list.
   *
   * @param array $data
   *   Provided data to update the contact with.
   * @param object $contact
   *   Contact match to provided data.
   * @param array $listIDs
   *   An array of list UUIDs that the contact is being updated on.
   *
   * @see https://sendpulse.com/integrations/api
   */
  private function updateContact(array $data, $contact, $listIDs) {
    $config = $this->getConfig();

    if ($contact && property_exists($contact, 'contact_id')) {

      $body = $contact;
      $body = $this->buildResponseBody($data, $body);

      // Add required fields.
      $body->email_address->address = $data['email_address'];
      $body->email_address->permission_to_send = 'implicit';
      $body->update_source = 'Contact';
      $body->list_memberships = $listIDs;

      $this->moduleHandler->invokeAll('sendpulse_api_contact_data_alter', [$data, &$body]);
      $this->moduleHandler->invokeAll('sendpulse_api_contact_update_data_alter', [$data, &$body]);

      try {
        $response = $this->httpClient->request('PUT', $config['contact_url'] . '/' . $contact->contact_id, [
          'headers' => [
            'Authorization' => 'Bearer ' . $config['api_secret'],
            'cache-control' => 'no-cache',
            'content-type' => 'application/json',
            'accept' => 'application/json',
          ],
          'body' => json_encode($body),
        ]);

        return $this->handleResponse($response, 'updateContact');

      }
      catch (RequestException $e) {
        // Return the error to show an error on form submission.
        return $this->handleRequestException($e);
      }
      catch (ClientException $e) {
        $this->handleRequestException($e);
      }
      catch (\Exception $e) {
        $this->loggerFactory->get('sendpulse_api')->error($e);

        // Return the error to show an error on form submission.
        return ['error' => $e];
      }
    }
    else {
      $this->loggerFactory->get('sendpulse_api')->error('error: No contact id provided for updateContact method');
      return ['error: No contact id provided'];
    }
  }

  /**
   * Updates token expiration time to 2 hours from now.
   *
   * Saves the new expiration time to the configuration.
   */
  protected function updateTokenExpiration() {
    $tokens = $this->config->getEditable('sendpulse_api.tokens');
    $tokens->set('expires', strtotime('now +2 hours'));
    $tokens->save();
  }

}
