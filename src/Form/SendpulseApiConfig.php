<?php

namespace Drupal\sendpulse_api\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface; // Correct FormStateInterface import
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\sendpulse_api\Service\SendpulseApi;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Drupal\Core\Config\TypedConfigManagerInterface; // Correct interface

/**
 * Class SendpulseApiConfig.
 *
 * Configuration form for adjusting content for the social feeds block.
 */
class SendpulseApiConfig extends ConfigFormBase {

  /**
   * Drupal\Core\Messenger\MessengerInterface.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   *   Messenger Interface.
   */
  protected $messenger;

  /**
   * Symfony\Component\HttpFoundation\RequestStack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * Drupal\sendpulse_api\Service\SendpulseApi.
   *
   * @var \Drupal\sendpulse_api\Service\SendpulseApi
   *   Sendpulse ems service.
   */
  protected $sendpulseApi;

  /**
   * SendpulseApiConfig constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Drupal\Core\Config\ConfigFactoryInterface.
   * @param \Drupal\Core\Config\TypedConfigManagerInterface $typed_config_manager
   *   Drupal\Core\Config\TypedConfigManagerInterface.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   Drupal\Core\Messenger\MessengerInterface.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   Symfony\Component\HttpFoundation\RequestStack.
   * @param \Drupal\sendpulse_api\Service\SendpulseApi $sendpulseApi
   *   Sendpulse ems service.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    TypedConfigManagerInterface $typed_config_manager,
    MessengerInterface $messenger,
    RequestStack $request_stack,
    SendpulseApi $sendpulseApi
  ) {
    // Pass the correct arguments to the parent constructor
    parent::__construct($config_factory, $typed_config_manager);

    // Initialize other dependencies
    $this->messenger = $messenger;
    $this->requestStack = $request_stack;
    $this->sendpulseApi = $sendpulseApi;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('config.typed'),
      $container->get('messenger'),
      $container->get('request_stack'),
      $container->get('sendpulse_api')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'sendpulse_api_configure';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'sendpulse_api.config',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $settings = $this->sendpulseApi->getConfig();
    $api_user_id = $settings['api_user_id'] ?? NULL;
    $api_secret = $settings['api_secret'] ?? NULL;
    $configType = $settings['config_type'] ?? 'config';

    if (version_compare(PHP_VERSION, '8.0', '<')) {
      $this->messenger->addMessage($this->t('The Sendpulse module is not compatible with your current version of PHP and should be upgraded to latest version or greater than 8.0.'), 'error');
    }

    $form['auth'] = [
      '#type' => 'details',
      '#title' => $this->t('Authorization Settings'),
      '#collapsible' => TRUE,
      '#open' => (!$api_secret),
    ];

    $form['auth']['message'] = [
      '#markup' => $configType === 'settings.php'
        ? $this->t('<p><strong>NOTE:</strong> Application settings were found in your <strong>settings.php</strong> file. Please update information
          there or remove to use this form.</p>')
        : $this->t('<p><strong>NOTE:</strong> Application settings are more
          secure when saved in your <strong>settings.php</strong> file. Please consider moving this information there. Example:</p><pre>  $settings[\'sendpulse_api\'] = [<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\'api_user_id\' => \'your_api_user_id\',<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\'api_secret\' => \'your_api_secret\',<br>];</pre>'),
    ];

    $form['auth']['api_user_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API User Id'),
      '#default_value' => $api_user_id ? $api_user_id : NULL,
      '#required' => TRUE,
      '#disabled' => $configType === 'settings.php' ? TRUE : FALSE,
    ];

    $form['auth']['api_secret'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API Secret'),
      '#default_value' => $api_secret ? $api_secret : NULL,
      '#required' => TRUE,
      '#disabled' => $configType === 'settings.php' ? TRUE : FALSE,
    ];

    $form = parent::buildForm($form, $form_state);

    if ($configType === 'settings.php') {
      unset($form['actions']['submit']);
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $config = $this->config('sendpulse_api.config');
    $api_user_id = $form_state->getValue('api_user_id');
    $api_secret = $form_state->getValue('api_secret');
    $config->clear('sendpulse_api.config');
    $config->set('api_user_id', $api_user_id);
    $config->set('api_secret', $api_secret);
    $config->save();

    $this->messenger->addMessage($this->t('Your configuration has been saved'));
  }

}
