<?php

namespace Drupal\sendpulse_api\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface; // Correct FormStateInterface import
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\sendpulse_api\Service\SendpulseApi;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Drupal\Core\Config\TypedConfigManagerInterface; // Correct interface

/**
 * Class SendpulseApiLists.
 *
 * Configuration form for adjusting content for the social feeds block.
 */
class SendpulseApiLists extends ConfigFormBase {

  /**
   * Drupal\Core\Messenger\MessengerInterface.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   *   Messenger Interface.
   */
  protected $messenger;

  /**
   * Symfony\Component\HttpFoundation\RequestStack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * Drupal\sendpulse_api\Service\SendpulseApi.
   *
   * @var \Drupal\sendpulse_api\Service\SendpulseApi
   *   Sendpulse ems service.
   */
  protected $sendpulseApi;

  /**
   * SendpulseApiLists constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Drupal\Core\Config\ConfigFactoryInterface.
   * @param \Drupal\Core\Config\TypedConfigManagerInterface $typed_config_manager
   *   Drupal\Core\Config\TypedConfigManagerInterface.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   Drupal\Core\Messenger\MessengerInterface.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   Symfony\Component\HttpFoundation\RequestStack.
   * @param \Drupal\sendpulse_api\Service\SendpulseApi $sendpulseApi
   *   Sendpulse ems service.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    TypedConfigManagerInterface $typed_config_manager,
    MessengerInterface $messenger,
    RequestStack $request_stack,
    SendpulseApi $sendpulseApi
  ) {
    // Pass the correct arguments to the parent constructor
    parent::__construct($config_factory, $typed_config_manager);

    // Initialize other dependencies
    $this->messenger = $messenger;
    $this->requestStack = $request_stack;
    $this->sendpulseApi = $sendpulseApi;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('config.typed'),
      $container->get('messenger'),
      $container->get('request_stack'),
      $container->get('sendpulse_api')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'sendpulse_api_lists';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'sendpulse_api.enabled_lists',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->sendpulseApi->getConfig();
    $enabled = $this->config('sendpulse_api.enabled_lists')->getRawData();

    $header = [
      'name' => $this->t('List Name'),
      'list_id' => $this->t('List UUID'),
    ];

    $output = $defaultValue = [];

    if (isset($config['api_secret'])) {
      $lists = $this->sendpulseApi->getMailingLists();

      if (is_array($lists)) {
        if ($lists && is_array($lists) &&count($lists) > 0) {
          foreach ($lists as $list_id => $list) {
            $output[$list_id] = [
              'name' => $list->name,
              'list_id' => $list_id,
            ];

            $defaultValue[$list_id] = isset($enabled[$list_id]) && $enabled[$list_id] === 1 ? $list_id : NULL;
          }
        }
      }
    }

    $form['lists'] = [
      '#type' => 'tableselect',
      '#header' => $header,
      '#options' => $output,
      '#default_value' => $defaultValue,
      '#empty' => $this->t('You must authorize Sendpulse Api before enabling a list or there are no lists available on your account.'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('sendpulse_api.enabled_lists');
    $config->clear('sendpulse_api.enabled_lists');
    $enabled = $form_state->getValues()['lists'];
    $lists = $this->sendpulseApi->getMailingLists();

    foreach ($enabled as $key => $value) {
      $config->set($key, ($value === 0 ? 0 : 1));
    }

    $config->save();

    $this->sendpulseApi->saveContactLists($lists);

    $this->messenger->addMessage($this->t('Your configuration has been saved'));
  }

}
