<?php

namespace Drupal\sendpulse_api\Plugin\WebformHandler;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Serialization\Yaml;
use Drupal\webform\Plugin\WebformHandlerBase;
use Drupal\webform\WebformSubmissionInterface;
use Drupal\webform\WebformTokenManagerInterface;
use Drupal\sendpulse_api\Service\SendpulseApi;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Cache\CacheBackendInterface;

/**
 * Form submission to Sendpulse Api handler.
 *
 * @WebformHandler(
 *   id = "sendpulse_api",
 *   label = @Translation("Sendpulse Api"),
 *   category = @Translation("Sendpulse Api"),
 *   description = @Translation("Sends a form submission to a Sendpulse Api list."),
 *   cardinality = \Drupal\webform\Plugin\WebformHandlerInterface::CARDINALITY_UNLIMITED,
 *   results = \Drupal\webform\Plugin\WebformHandlerInterface::RESULTS_PROCESSED,
 * )
 */
class WebformSendpulseApiHandler extends WebformHandlerBase {

  /**
   * Drupal\Core\Cache\CacheBackendInterface.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   *   Drupal cache.
   */
  protected $cache;

  /**
   * Drupal\sendpulse_api\Service\SendpulseApi.
   *
   * @var \Drupal\sendpulse_api\Service\SendpulseApi
   *   Sendpulse ems service.
   */
  protected $sendpulseApi;

  /**
   * The token manager.
   *
   * @var \Drupal\webform\WebformTokenManagerInterface
   */
  protected $tokenManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create(
      $container,
      $configuration,
      $plugin_id,
      $plugin_definition
    );
    $instance->setCacheManager($container->get('cache.default'));
    // $instance->setTokenManager($container->get('webform.tokenManager'));
    $instance->setTokenManager($container->get('webform.token_manager'));
    $instance->setSendpulseApi($container->get('sendpulse_api'));
    return $instance;
  }

  /**
   * Set Cache dependency.
   */
  protected function setCacheManager(CacheBackendInterface $cache) {
    $this->cache = $cache;
  }

  /**
   * Set Token Manager dependency.
   */
  protected function setTokenManager(WebformTokenManagerInterface $tokenManager) {
    $this->tokenManager = $tokenManager;
  }

  /**
   * Set Sendpulse Api dependency.
   */
  protected function setSendpulseApi(SendpulseApi $sendpulseApi) {
    $this->sendpulseApi = $sendpulseApi;
  }

  /**
   * {@inheritdoc}
   */
  public function getSummary() {
    $fields = $this->getWebform()->getElementsInitializedAndFlattened();
    $lists = $this->sendpulseApi->getMailingLists();

    $email_summary = $this->configuration['email'];
    if (!empty($fields[$this->configuration['email']])) {
      $email_summary = $fields[$this->configuration['email']]['#title'];
    }
    $email_summary = $this->t('<strong>Email</strong>: @email_summary', ['@email_summary' => $email_summary]);

    $list_summary = $this->configuration['list'];
    if (!empty($lists[$this->configuration['list']])) {
      $list_summary = $lists[$this->configuration['list']]->name;
    }
    $list_summary = $this->t('<strong>List</strong>: @list_summary', ['@list_summary' => $list_summary]);

    $markup = "$email_summary<br/>$list_summary";
    return [
      '#markup' => $markup,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'list' => '',
      'email' => '',
      'name' => '',
      'phone' => '',
      'mergevars' => '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $enabled = $this->configFactory->get('sendpulse_api.enabled_lists')->getRawData();
    $lists = $this->sendpulseApi->getMailingLists();

    $options = [];
    foreach ($lists as $list_id => $list) {
      if ($enabled[$list_id] == 1) {
        $options[$list_id] = $list->name;
      }
    }

    $form['sendpulse_api'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Sendpulse Api settings'),
      '#attributes' => ['id' => 'webform-sendpulse-api-handler-settings'],
    ];

    $form['sendpulse_api']['update'] = [
      '#type' => 'submit',
      '#value' => $this->t('Refresh lists & groups'),
      '#ajax' => [
        'callback' => [$this, 'ajaxSendpulseApiListHandler'],
        'wrapper' => 'webform-sendpulse-api-handler-settings',
      ],
      '#submit' => [[get_class($this), 'sendpulseapiUpdateConfigSubmit']],
    ];

    $form['sendpulse_api']['list'] = [
      '#type' => 'webform_select_other',
      '#title' => $this->t('List'),
      '#required' => TRUE,
      '#empty_option' => $this->t('- Select an option -'),
      '#default_value' => $this->configuration['list'],
      '#options' => $options,
      '#ajax' => [
        'callback' => [$this, 'ajaxSendpulseApiListHandler'],
        'wrapper' => 'webform-sendpulse-api-handler-settings',
      ],
      '#description' => $this->t('Select the list you want to send this submission to. Alternatively, you can also use the Other field for token replacement.'),
    ];

    $fields = $this->getWebform()->getElementsInitializedAndFlattened();
    $custom_fields = $this->sendpulseApi->getCustomFields(FALSE);
    $custom_fields = $custom_fields->custom_fields;
    $options = [];
    foreach ($fields as $field_name => $field) {
      if (in_array($field['#type'], ['email', 'webform_email_confirm'])) {
        $options[$field_name] = $field['#title'];
      }
    }

    $default_value = $this->configuration['email'];
    if (empty($this->configuration['email']) && count($options) == 1) {
      $default_value = key($options);
    }
    $form['sendpulse_api']['email'] = [
      '#type' => 'webform_select_other',
      '#title' => $this->t('Email field'),
      '#required' => TRUE,
      '#default_value' => $default_value,
      '#options' => $options,
      '#empty_option' => $this->t('- Select an option -'),
      '#description' => $this->t('Select the email element you want to use for subscribing to the Sendpulse Api list specified above. Alternatively, you can also use the Other field for token replacement.'),
    ];

    if ($custom_fields) {
      $custom_options = [];
      foreach ($fields as $field_name => $field) {
        if (!in_array($field['#type'], ['email', 'webform_email_confirm'])) {
          $custom_options[$field_name] = $field['#title'];
        }
      }

      foreach ($custom_fields as $custom_field) {
        $form['sendpulse_api'][$custom_field->custom_field_id] = [
          '#type' => 'webform_select_other',
          '#title' => $this->t('@label', ['@label' => $custom_field->label]),
          '#required' => FALSE,
          '#options' => $custom_options,
          '#default_value' => $this->configuration[$custom_field->custom_field_id],
          '#description' => $this->t('Select the email element you want to use for subscribing to the Sendpulse Api list specified above. Alternatively, you can also use the Other field for token replacement.'),
        ];
      }
    }

    $form['sendpulse_api']['mergevars'] = [
      '#type' => 'webform_codemirror',
      '#mode' => 'yaml',
      '#title' => $this->t('Merge vars'),
      '#default_value' => $this->configuration['mergevars'],
      '#description' => $this->t('You can map additional fields from your webform to fields in your Sendpulse Api list, one per line. An example might be name: [webform_submission:values:name]. The result is sent as an array. You may use tokens.'),
    ];

    $form['sendpulse_api']['token_tree_link'] = $this->tokenManager->buildTreeLink();

    return $form;
  }

  /**
   * Ajax callback to update Webform Sendpulse Api settings.
   */
  public static function ajaxSendpulseApiListHandler(array $form, FormStateInterface $form_state) {
    return $form['settings']['sendpulse_api'];
  }

  /**
   * Submit callback for the refresh button.
   */
  public static function sendpulseapiUpdateConfigSubmit(array $form, FormStateInterface $form_state) {
    // Trigger list and group category refetch by deleting lists cache.
    $cache = \Drupal::cache();
    $cache->delete('sendpulse_api.lists');
    $form_state->setRebuild();
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    $values = $form_state->getValues();
    foreach ($this->configuration as $name => $value) {
      if (isset($values['sendpulse_api'][$name])) {
        $this->configuration[$name] = $values['sendpulse_api'][$name];
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function postSave(WebformSubmissionInterface $webform_submission, $update = TRUE) {
    // If update, do nothing.
    if ($update) {
      return;
    }

    $fields = $webform_submission->toArray(TRUE);

    $configuration = $this->tokenManager->replace($this->configuration, $webform_submission);

    // Check if we are using a token for the list field & replace value.
    if (isset($this->configuration['list']) && strpos($this->configuration['list'], '[webform_submission:values:') !== FALSE) {
      $configuration['list'] = NULL;
      $fieldToken = str_replace(['[webform_submission:values:', ']'], '', $this->configuration['list']);

      if (isset($fields['data'][$fieldToken])) {
        if (is_string($fields['data'][$fieldToken])) {
          $configuration['list'] = [$fields['data'][$fieldToken]];
        }
        elseif (is_array($fields['data'][$fieldToken])) {
          $configuration['list'] = $fields['data'][$fieldToken];
        }
      }
    }

    // Email could be a webform element or a string/token.
    if (!empty($fields['data'][$configuration['email']])) {
      $email = $fields['data'][$configuration['email']];
    }
    else {
      $email = $configuration['email'];
    }

    $mergevars = Yaml::decode($configuration['mergevars']) ?? [];

    // Allow other modules to alter the merge vars.
    // @see hook_sendpulse_api_lists_mergevars_alter().
    $entity_type = 'webform_submission';
    \Drupal::moduleHandler()->alter('sendpulse_api_lists_mergevars', $mergevars, $webform_submission, $entity_type);
    \Drupal::moduleHandler()->alter('webform_sendpulse_api_lists_mergevars', $mergevars, $webform_submission, $this);

    $handler_link = Link::createFromRoute(
      $this->t('Edit handler'),
      'entity.webform.handler.edit_form',
      [
        'webform' => $this->getWebform()->id(),
        'webform_handler' => $this->getHandlerId(),
      ]
    )->toString();

    $submission_link = ($webform_submission->id()) ? $webform_submission->toLink($this->t('View'))->toString() : NULL;

    $context = [
      'link' => $submission_link . ' / ' . $handler_link,
      'webform_submission' => $webform_submission,
      'handler_id' => $this->getHandlerId(),
    ];

    if (!empty($configuration['list']) && !empty($email)) {
      $data = array_merge(['email_address' => $email], $mergevars);
      $custom_fields = $this->sendpulseApi->getCustomFields(FALSE);
      $custom_fields = $custom_fields->custom_fields;
      if ($custom_fields) {
        $fields_data = $fields['data'] ?? [];
        foreach ($custom_fields as $custom_field) {
          $field_name = $custom_field->custom_field_id;
          $data[$field_name] = (isset($fields_data[$configuration[$field_name]])) ? $fields_data[$configuration[$field_name]] : $configuration[$field_name];
        }
      }

      $this->sendpulseApi->submitContactForm($data, $configuration['list']);
    }
    else {
      if (empty($configuration['list'])) {
        \Drupal::logger('webform_submission')->warning(
        'No Sendpulse Api list was provided to the handler.',
        $context
        );
      }
      if (empty($email)) {
        \Drupal::logger('webform_submission')->warning(
        'No email address was provided to the handler.',
        $context
        );
      }
    }
  }

}
