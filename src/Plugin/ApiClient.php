<?php

namespace Drupal\sendpulse_api\Plugin;

use Drupal\Core\Logger\LoggerChannelFactoryInterface;

/**
 * Client for interacting with the SendPulse API.
 *
 * This class implements the ApiInterface and provides methods for communicating
 * with the SendPulse API. It handles API requests and responses for operations
 * such as managing address books, campaigns, emails, and more. It uses the
 * SendPulse API endpoints to perform tasks such as sending campaigns, managing
 * subscriptions, and retrieving campaign statistics.
 */
class ApiClient implements ApiInterface {

  /**
   * The base URL for the SendPulse API.
   *
   * @var string
   */
  private $apiUrl = 'https://api.sendpulse.com';

  /**
   * The User ID for the SendPulse API.
   *
   * @var string
   */
  private $userId;

  /**
   * The secret key for the SendPulse API.
   *
   * @var string
   */
  private $secret;

  /**
   * The authentication token for the SendPulse API.
   *
   * @var string|null
   */
  private $token;

  /**
   * Flag indicating whether the refresh token is needed.
   *
   * @var int
   */
  private $refreshToken = 0;

  /**
   * Flag indicating whether to retry a request.
   *
   * @var bool
   */
  private $retry = FALSE;

  /**
   * Token storage service for managing tokens.
   *
   * @var null|TokenStorageInterface
   */
  private $tokenStorage;

  /**
   * Logger service for logging messages and errors.
   *
   * @var \Psr\Log\LoggerInterface
   */
  private $logger;

  /**
   * Sendpulse API constructor.
   *
   * Initializes the ApiClient object with the provided user ID
   * secret, token storage, and logger.
   * If no token storage is provided, an empty value is set.
   * If either the user ID or secret is missing, an error is logged,
   * and the constructor returns early. The constructor attempts to
   * load a token from the storage or retrieve it using the `getToken()`
   * method. If token retrieval
   * fails, an error is logged, and the constructor exits early.
   *
   * @param string $userId
   *   The user ID for the SendPulse API.
   * @param string $secret
   *   The secret key for the SendPulse API.
   * @param TokenStorageInterface|null $tokenStorage
   *   The token storage service (optional, defaults to NULL). If not
   *   provided, an empty value is set.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $loggerFactory
   *   The logger channel factory to create a logger for error logging.
   */
  public function __construct($userId, $secret, ?TokenStorageInterface $tokenStorage = NULL, LoggerChannelFactoryInterface $loggerFactory) {
    if ($tokenStorage === NULL) {
      // $tokenStorage = new FileStorage();
      $tokenStorage = '';
    }
    if (empty($userId) || empty($secret)) {
      // Get the logger service for the 'sendpulse' channel and log an error.
      $this->logger = $loggerFactory->get('sendpulse');
      $this->logger->error('Empty ID or SECRET provided');
      // Return early if ID or SECRET is missing.
      return;
    }

    $this->userId = $userId;
    $this->secret = $secret;
    $this->tokenStorage = $tokenStorage;

    // $hashName = md5($userId . '::' . $secret);
    // $this->token = $this->tokenStorage->get($hashName);
    $this->token = '';

    if (empty($this->token) && !$this->getToken()) {

      // Log the failure to connect.
      $this->logger = $loggerFactory->get('sendpulse');
      $this->logger->error('Could not connect to API, check your ID and SECRET');
      // Return early if token retrieval failed.
      return;
    }
  }

  /**
   * Get token and store it.
   *
   * @return bool
   *   `true` if the token was successfully retrieved, `false` otherwise.
   */
  private function getToken() {
    $data = [
      'grant_type' => 'client_credentials',
      'client_id' => $this->userId,
      'client_secret' => $this->secret,
    ];

    $requestResult = $this->sendRequest('oauth/access_token', 'POST', $data, FALSE);

    if ($requestResult->http_code !== 200) {
      return FALSE;
    }

    $this->refreshToken = 0;
    $this->token = $requestResult->data->access_token;

    // $hashName = md5($this->userId . '::' . $this->secret);
    // $this->tokenStorage->set($hashName, $this->token);
    return TRUE;
  }

  /**
   * Form and send request to API service.
   *
   * This method constructs and sends a request to the API service,
   * returning the response
   * as a `\stdClass` object. The returned object will contain the
   * response data or error information depending on the API's response.
   *
   * @param string $path
   *   The API endpoint path.
   * @param string $method
   *   The HTTP method to use (e.g., 'GET', 'POST'). Defaults to 'GET'.
   * @param array $data
   *   The data to send with the request (used for 'POST' and other
   *   methods). Defaults to an empty array.
   * @param bool $useToken
   *   Whether to use an authentication token for the request.
   *   Defaults to `TRUE`.
   *
   * @return \stdClass
   *   The response from the API service, typically containing data
   *   or an error message.
   */
  protected function sendRequest($path, $method = 'GET', $data = [], $useToken = TRUE) {
    $url = $this->apiUrl . '/' . $path;
    $method = strtoupper($method);
    $curl = curl_init();

    if ($useToken && !empty($this->token)) {
      $headers = ['Authorization: Bearer ' . $this->token, 'Expect:'];
      curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
    }

    switch ($method) {
      case 'POST':
        curl_setopt($curl, CURLOPT_POST, count($data));
        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
        break;

      case 'PUT':
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'PUT');
        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
        break;

      case 'DELETE':
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'DELETE');
        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
        break;

      default:
        if (!empty($data)) {
          $url .= '?' . http_build_query($data);
        }
    }

    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, TRUE);
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, FALSE);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($curl, CURLOPT_HEADER, TRUE);
    curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 300);
    curl_setopt($curl, CURLOPT_TIMEOUT, 300);

    $response = curl_exec($curl);
    $header_size = curl_getinfo($curl, CURLINFO_HEADER_SIZE);
    $headerCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
    $responseBody = substr($response, $header_size);
    $responseHeaders = substr($response, 0, $header_size);
    $ip = curl_getinfo($curl, CURLINFO_PRIMARY_IP);
    $curlErrors = curl_error($curl);

    curl_close($curl);

    if ($headerCode === 401 && $this->refreshToken === 0) {
      ++$this->refreshToken;
      $this->getToken();
      $retval = $this->sendRequest($path, $method, $data);
    }
    else {
      $retval = new \stdClass();
      $retval->data = json_decode($responseBody);
      $retval->http_code = $headerCode;
      $retval->headers = $responseHeaders;
      $retval->ip = $ip;
      $retval->curlErrors = $curlErrors;
      $retval->method = $method . ':' . $url;
      $retval->timestamp = date('Y-m-d h:i:sP');
    }

    return $retval;
  }

  /**
   * Process results.
   *
   * This method processes the data and returns a `\stdClass` object
   * containing the processed result.
   *
   * @param mixed $data
   *   The data to be processed, which could be of any type depending
   *   on the method's use.
   *
   * @return \stdClass
   *   A `\stdClass` object containing the processed result, which can
   *   include success status, response data, or error details.
   */
  protected function handleResult($data) {
    if (empty($data->data)) {
      $data->data = new \stdClass();
    }
    if ($data->http_code !== 200) {
      $data->data->is_error = TRUE;
      $data->data->http_code = $data->http_code;
      $data->data->headers = $data->headers;
      $data->data->curlErrors = $data->curlErrors;
      $data->data->ip = $data->ip;
      $data->data->method = $data->method;
      $data->data->timestamp = $data->timestamp;
    }

    return $data->data;
  }

  /**
   * Process errors.
   *
   * This method processes error information and returns a `\stdClass`
   * object containing
   * error status and an optional custom error message.
   *
   * @param string|null $customMessage
   *   An optional custom error message. If provided, it will be added
   *   to the response.
   *   Defaults to `NULL`.
   *
   * @return \stdClass
   *   A `\stdClass` object containing the error status (`is_error`
   *   set to `TRUE`) and
   *   an optional error message (`message`).
   */
  protected function handleError($customMessage = NULL) {
    $message = new \stdClass();
    $message->is_error = TRUE;
    if (NULL !== $customMessage) {
      $message->message = $customMessage;
    }

    return $message;
  }

  /**
   * Create a new address book.
   *
   * This method creates a new address book with the provided name.
   * If the book name is empty, an error message is returned. Otherwise,
   * it sends a request to create the address book and processes
   * the response.
   *
   * @param string $bookName
   *   The name of the address book to be created.
   *
   * @return \stdClass
   *   A `\stdClass` object representing the result of the request.
   *   If the address book creation is successful, the object will
   *   contain the result data.
   *   If there's an error (e.g., empty book name), the object will
   *   contain an error message.
   */
  public function createAddressBook($bookName) {
    if (empty($bookName)) {
      return $this->handleError('Empty book name');
    }

    $data = ['bookName' => $bookName];
    $requestResult = $this->sendRequest('addressbooks', 'POST', $data);

    return $this->handleResult($requestResult);
  }

  /**
   * Edit the name of an existing address book.
   *
   * This method updates the name of an address book with the specified
   * ID. If the book ID or the new name is empty, an error message will
   * be returned. Otherwise, it sends a request to update the address
   * book and processes the response.
   *
   * @param int|string $id
   *   The ID of the address book to be edited.
   * @param string $newName
   *   The new name to be assigned to the address book.
   *
   * @return \stdClass
   *   A `\stdClass` object representing the result of the request.
   *   If the address book name update is successful, the object will
   *   contain the result data. If there's an error (e.g.,
   *   missing book ID or new name), the object will contain an
   *   error message.
   */
  public function editAddressBook($id, $newName) {
    if (empty($newName) || empty($id)) {
      return $this->handleError('Empty new name or book id');
    }

    $data = ['name' => $newName];
    $requestResult = $this->sendRequest('addressbooks/' . $id, 'PUT', $data);

    return $this->handleResult($requestResult);
  }

  /**
   * Remove an existing address book.
   *
   * This method deletes an address book identified by the provided ID.
   * If the book ID is empty, an error message will be returned.
   * Otherwise, it sends a request to delete the address book and
   * processes the response.
   *
   * @param int|string $id
   *   The ID of the address book to be removed.
   *
   * @return \stdClass
   *   A `\stdClass` object representing the result of the request.
   *   If the address book removal is successful, the object will
   *   contain the result data. If there's an error (e.g.,
   *   missing book ID), the object will contain an error message.
   */
  public function removeAddressBook($id) {
    if (empty($id)) {
      return $this->handleError('Empty book id');
    }

    $requestResult = $this->sendRequest('addressbooks/' . $id, 'DELETE');

    return $this->handleResult($requestResult);
  }

  /**
   * Get a list of address books.
   *
   * This method retrieves a list of address books from the API.
   * You can specify the maximum number of address books to retrieve
   * (`$limit`) and the starting point for the results (`$offset`)
   * for pagination.
   *
   * @param int|null $limit
   *   The maximum number of address books to retrieve (optional).
   *   If not provided, the API may return a default number.
   * @param int|null $offset
   *   The offset (starting point) for pagination (optional). If not
   *   provided, the results will start from the beginning.
   *
   * @return \stdClass
   *   A `\stdClass` object containing the result of the request.
   *   It may include a list of address books or an error message
   *   if something went wrong.
   */
  public function listAddressBooks($limit = NULL, $offset = NULL) {
    $data = [];
    if (NULL !== $limit) {
      $data['limit'] = $limit;
    }
    if (NULL !== $offset) {
      $data['offset'] = $offset;
    }

    $requestResult = $this->sendRequest('addressbooks', 'GET', $data);

    return $this->handleResult($requestResult);
  }

  /**
   * Get information about a book.
   *
   * @param string|int $id
   *   The unique identifier for the book.
   *
   * @return \stdClass
   *   The book information as an object.
   */
  public function getBookInfo($id) {
    if (empty($id)) {
      return $this->handleError('Empty book id');
    }

    $requestResult = $this->sendRequest('addressbooks/' . $id);

    return $this->handleResult($requestResult);
  }

  /**
   * Get variables from book.
   *
   * @param string|int $id
   *   Address book id.
   *
   * @return \stdClass
   *   The variables of the book as an object.
   */
  public function getBookVariables($id) {
    if (empty($id)) {
      return $this->handleError('Empty book id');
    }

    $requestResult = $this->sendRequest('addressbooks/' . $id . '/variables');

    return $this->handleResult($requestResult);
  }

  /**
   * Change varible by user email.
   *
   * @param int $bookID
   *   The book id.
   * @param string $email
   *   User email.
   * @param array $vars
   *   User vars in [key=>value] format.
   *
   * @return \stdClass
   *   The variables of the book as an object.
   */
  public function updateEmailVariables(int $bookID, string $email, array $vars) {
    if (empty($bookID)) {
      return $this->handleError('Empty book id');
    }

    $data = ['email' => $email, 'variables' => []];
    foreach ($vars as $name => $val) {
      $data['variables'][] = ['name' => $name, 'value' => $val];
    }

    $requestResult = $this->sendRequest('addressbooks/' . $bookID . '/emails/variable', 'POST', $data);

    return $this->handleResult($requestResult);
  }

  /**
   * Retrieve emails associated with a specific book.
   *
   * @param string|int $id
   *   The unique identifier of the book.
   * @param int|null $limit
   *   The maximum number of emails to retrieve (optional).
   * @param int|null $offset
   *   The offset for pagination (optional).
   *
   * @return \stdClass
   *   The list of emails from the book as an object.
   */
  public function getEmailsFromBook($id, $limit = NULL, $offset = NULL) {
    if (empty($id)) {
      return $this->handleError('Empty book id');
    }

    $data = [];
    if (NULL !== $limit) {
      $data['limit'] = $limit;
    }
    if (NULL !== $offset) {
      $data['offset'] = $offset;
    }

    $requestResult = $this->sendRequest('addressbooks/' . $id . '/emails', 'GET', $data);

    return $this->handleResult($requestResult);
  }

  /**
   * Add emails to a specific address book.
   *
   * @param string|int $bookID
   *   The unique identifier of the address book.
   * @param array $emails
   *   An array of emails to be added to the address book.
   * @param array $additionalParams
   *   Optional additional parameters to include in the request.
   *
   * @return \stdClass
   *   The result of the add operation as an object.
   */
  public function addEmails($bookID, $emails, $additionalParams = []) {
    if (empty($bookID) || empty($emails)) {
      return $this->handleError('Empty book id or emails');
    }

    $data = [
      'emails' => json_encode($emails),
    ];

    if ($additionalParams) {
      $data = array_merge($data, $additionalParams);
    }

    $requestResult = $this->sendRequest('addressbooks/' . $bookID . '/emails', 'POST', $data);

    return $this->handleResult($requestResult);
  }

  /**
   * Remove emails from a specific address book.
   *
   * @param string|int $bookID
   *   The unique identifier of the address book.
   * @param array $emails
   *   An array of emails to be removed from the address book.
   *
   * @return \stdClass
   *   The result of the remove operation as an object.
   */
  public function removeEmails($bookID, $emails) {
    if (empty($bookID) || empty($emails)) {
      return $this->handleError('Empty book id or emails');
    }

    $data = [
      'emails' => serialize($emails),
    ];

    $requestResult = $this->sendRequest('addressbooks/' . $bookID . '/emails', 'DELETE', $data);

    return $this->handleResult($requestResult);
  }

  /**
   * Retrieve information about a specific email in an address book.
   *
   * @param string|int $bookID
   *   The unique identifier of the address book.
   * @param string $email
   *   The email address to retrieve information for.
   *
   * @return \stdClass
   *   The email information as an object.
   */
  public function getEmailInfo($bookID, $email) {
    if (empty($bookID) || empty($email)) {
      return $this->handleError('Empty book id or email');
    }

    $requestResult = $this->sendRequest('addressbooks/' . $bookID . '/emails/' . $email);

    return $this->handleResult($requestResult);
  }

  /**
   * Retrieve the cost of a campaign for a specific address book.
   *
   * @param string|int $bookID
   *   The unique identifier of the address book.
   *
   * @return \stdClass
   *   The campaign cost information as an object.
   */
  public function campaignCost($bookID) {
    if (empty($bookID)) {
      return $this->handleError('Empty book id');
    }

    $requestResult = $this->sendRequest('addressbooks/' . $bookID . '/cost');

    return $this->handleResult($requestResult);
  }

  /**
   * Retrieve a list of campaigns with optional pagination.
   *
   * @param int|null $limit
   *   The maximum number of campaigns to retrieve (optional).
   * @param int|null $offset
   *   The offset for pagination (optional).
   *
   * @return \stdClass
   *   The list of campaigns as an object.
   */
  public function listCampaigns($limit = NULL, $offset = NULL) {
    $data = [];
    if (!empty($limit)) {
      $data['limit'] = $limit;
    }
    if (!empty($offset)) {
      $data['offset'] = $offset;
    }
    $requestResult = $this->sendRequest('campaigns', 'GET', $data);

    return $this->handleResult($requestResult);
  }

  /**
   * Retrieve information about a specific campaign.
   *
   * @param string|int $id
   *   The unique identifier of the campaign.
   *
   * @return \stdClass
   *   The campaign information as an object.
   */
  public function getCampaignInfo($id) {
    if (empty($id)) {
      return $this->handleError('Empty campaign id');
    }

    $requestResult = $this->sendRequest('campaigns/' . $id);

    return $this->handleResult($requestResult);
  }

  /**
   * Retrieve the campaign statistics by countries for a specific campaign.
   *
   * @param string|int $id
   *   The unique identifier of the campaign.
   *
   * @return \stdClass
   *   The campaign statistics by countries as an object.
   */
  public function campaignStatByCountries($id) {
    if (empty($id)) {
      return $this->handleError('Empty campaign id');
    }

    $requestResult = $this->sendRequest('campaigns/' . $id . '/countries');

    return $this->handleResult($requestResult);
  }

  /**
   * Retrieve the campaign statistics by referrals for a specific campaign.
   *
   * @param string|int $id
   *   The unique identifier of the campaign.
   *
   * @return \stdClass
   *   The campaign statistics by referrals as an object.
   */
  public function campaignStatByReferrals($id) {
    if (empty($id)) {
      return $this->handleError('Empty campaign id');
    }

    $requestResult = $this->sendRequest('campaigns/' . $id . '/referrals');

    return $this->handleResult($requestResult);
  }

  /**
   * Create a new campaign.
   *
   * @param string $senderName
   *   The name of the sender.
   * @param string $senderEmail
   *   The email address of the sender.
   * @param string $subject
   *   The subject of the campaign.
   * @param string|int $bodyOrTemplateId
   *   The body content of the campaign or template ID to use.
   * @param string|int $bookId
   *   The unique identifier of the address book to send the campaign to.
   * @param string $name
   *   The name of the campaign (optional).
   * @param array $attachments
   *   An array of file attachments (optional).
   * @param string $type
   *   The type of campaign (optional).
   * @param bool $useTemplateId
   *   Flag to indicate whether to use a template ID (optional).
   * @param string $sendDate
   *   The date to send the campaign (optional).
   * @param int|null $segmentId
   *   The segment ID to target specific recipients (optional).
   * @param array $attachmentsBinary
   *   An array of binary file attachments (optional).
   *
   * @return mixed
   *   The result of the campaign creation, typically an object or a response.
   */
  public function createCampaign(
    $senderName,
    $senderEmail,
    $subject,
    $bodyOrTemplateId,
    $bookId,
    $name = '',
    $attachments = [],
    $type = '',
    $useTemplateId = FALSE,
    $sendDate = '',
    $segmentId = NULL,
    $attachmentsBinary = [],
  ) {
    if (empty($senderName) || empty($senderEmail) || empty($subject) || empty($bodyOrTemplateId) || empty($bookId)) {
      return $this->handleError('Not all data.');
    }

    if ($useTemplateId) {
      $paramName = 'template_id';
      $paramValue = $bodyOrTemplateId;
    }
    else {
      $paramName = 'body';
      $paramValue = base64_encode($bodyOrTemplateId);
    }

    $data = [
      'sender_name' => $senderName,
      'sender_email' => $senderEmail,
      'subject' => $subject,
      $paramName => $paramValue,
      'list_id' => $bookId,
      'name' => $name,
      'type' => $type,
    ];

    if (!empty($attachments)) {
      $data['attachments'] = $attachments;
    }
    elseif (!empty($attachmentsBinary)) {
      $data['attachments_binary'] = $attachmentsBinary;
    }

    if (!empty($sendDate)) {
      $data['send_date'] = $sendDate;
    }

    if (!empty($segmentId)) {
      $data['segment_id'] = $segmentId;
    }

    $requestResult = $this->sendRequest('campaigns', 'POST', $data);

    return $this->handleResult($requestResult);
  }

  /**
   * Cancel a specific campaign.
   *
   * @param string|int $id
   *   The unique identifier of the campaign.
   *
   * @return \stdClass
   *   The result of the cancellation as an object.
   */
  public function cancelCampaign($id) {
    if (empty($id)) {
      return $this->handleError('Empty campaign id');
    }

    $requestResult = $this->sendRequest('campaigns/' . $id, 'DELETE');

    return $this->handleResult($requestResult);
  }

  /**
   * Retrieve a list of all senders.
   *
   * @return mixed
   *   The list of senders, typically as an object or array.
   */
  public function listSenders() {
    $requestResult = $this->sendRequest('senders');

    return $this->handleResult($requestResult);
  }

  /**
   * Retrieve a list of SMS senders.
   *
   * @return mixed
   *   The list of SMS senders, typically as an object or array,
   *   depending on the response format.
   */
  public function listSmsSenders() {
    $requestResult = $this->sendRequest('sms/senders');

    return $this->handleResult($requestResult);
  }

  /**
   * Add a new sender to the system.
   *
   * @param string $senderName
   *   The name of the sender.
   * @param string $senderEmail
   *   The email address of the sender.
   *
   * @return \stdClass
   *   The result of the add operation as an object.
   */
  public function addSender($senderName, $senderEmail) {
    if (empty($senderName) || empty($senderEmail)) {
      return $this->handleError('Empty sender name or email');
    }

    $data = [
      'email' => $senderEmail,
      'name' => $senderName,
    ];

    $requestResult = $this->sendRequest('senders', 'POST', $data);

    return $this->handleResult($requestResult);
  }

  /**
   * Remove an existing sender from the system by their email address.
   *
   * @param string $email
   *   The email address of the sender to be removed.
   *
   * @return \stdClass
   *   The result of the remove operation as an object.
   */
  public function removeSender($email) {
    if (empty($email)) {
      return $this->handleError('Empty email');
    }

    $data = [
      'email' => $email,
    ];

    $requestResult = $this->sendRequest('senders', 'DELETE', $data);

    return $this->handleResult($requestResult);
  }

  /**
   * Activate a sender using their email address and activation code.
   *
   * @param string $email
   *   The email address of the sender to be activated.
   * @param string $code
   *   The activation code sent to the sender.
   *
   * @return \stdClass
   *   The result of the activation operation as an object.
   */
  public function activateSender($email, $code) {
    if (empty($email) || empty($code)) {
      return $this->handleError('Empty email or activation code');
    }

    $data = [
      'code' => $code,
    ];

    $requestResult = $this->sendRequest('senders/' . $email . '/code', 'POST', $data);

    return $this->handleResult($requestResult);
  }

  /**
   * Request a mail with an activation code for a sender.
   *
   * @param string $email
   *   The email address of the sender for whom the activation code
   *   is requested.
   *
   * @return \stdClass
   *   The result of the request as an object.
   */
  public function getSenderActivationMail($email) {
    if (empty($email)) {
      return $this->handleError('Empty email');
    }

    $requestResult = $this->sendRequest('senders/' . $email . '/code');

    return $this->handleResult($requestResult);
  }

  /**
   * Retrieve global information for a specific email address.
   *
   * @param string $email
   *   The email address for which to retrieve global information.
   *
   * @return \stdClass
   *   The global information associated with the email as an object.
   */
  public function getEmailGlobalInfo($email) {
    if (empty($email)) {
      return $this->handleError('Empty email');
    }

    $requestResult = $this->sendRequest('emails/' . $email);

    return $this->handleResult($requestResult);
  }

  /**
   * Retrieve global information for a list of email addresses.
   *
   * @param array $emails
   *   An array of email addresses for which to retrieve global information.
   *
   * @return \stdClass
   *   The global information associated with the list of emails as an object.
   */
  public function getEmailsGlobalInfo($emails) {
    if (empty($emails)) {
      return $this->handleError('Empty emails list');
    }

    $requestResult = $this->sendRequest('emails', 'POST', $emails);

    return $this->handleResult($requestResult);
  }

  /**
   * Remove an email address from all address books.
   *
   * @param string $email
   *   The email address to be removed from all address books.
   *
   * @return \stdClass
   *   The result of the removal operation as an object.
   */
  public function removeEmailFromAllBooks($email) {
    if (empty($email)) {
      return $this->handleError('Empty email');
    }

    $requestResult = $this->sendRequest('emails/' . $email, 'DELETE');

    return $this->handleResult($requestResult);
  }

  /**
   * Retrieve email statistics.
   *
   * For all campaigns associated with a specific email address.
   *
   * @param string $email
   *   The email address for which to retrieve campaign statistics.
   *
   * @return \stdClass
   *   The campaign statistics associated with the email as an object.
   */
  public function emailStatByCampaigns($email) {
    if (empty($email)) {
      return $this->handleError('Empty email');
    }

    $requestResult = $this->sendRequest('emails/' . $email . '/campaigns');

    return $this->handleResult($requestResult);
  }

  /**
   * Retrieve all email addresses from the blacklist.
   *
   * @return mixed
   *   The list of blacklisted emails, typically as an
   *   object or array, depending on the response format.
   */
  public function getBlackList() {
    $requestResult = $this->sendRequest('blacklist');

    return $this->handleResult($requestResult);
  }

  /**
   * Add one or more email addresses to the blacklist.
   *
   * @param string $emails
   *   A string containing email addresses, separated by commas.
   * @param string $comment
   *   A comment or reason for blacklisting the email(s) (optional).
   *
   * @return \stdClass
   *   The result of the operation as an object.
   */
  public function addToBlackList($emails, $comment = '') {
    if (empty($emails)) {
      return $this->handleError('Empty email');
    }

    $data = [
      'emails' => base64_encode($emails),
      'comment' => $comment,
    ];

    $requestResult = $this->sendRequest('blacklist', 'POST', $data);

    return $this->handleResult($requestResult);
  }

  /**
   * Remove one or more email addresses from the blacklist.
   *
   * @param string $emails
   *   A string containing email addresses, separated by commas.
   *
   * @return \stdClass
   *   The result of the operation as an object.
   */
  public function removeFromBlackList($emails) {
    if (empty($emails)) {
      return $this->handleError('Empty email');
    }

    $data = [
      'emails' => base64_encode($emails),
    ];

    $requestResult = $this->sendRequest('blacklist', 'DELETE', $data);

    return $this->handleResult($requestResult);
  }

  /**
   * Retrieve the balance for a specific currency or the default balance.
   *
   * @param string $currency
   *   The currency for which to retrieve the balance (optional).
   *   If not provided, the default balance is returned.
   *
   * @return mixed
   *   The balance information, typically as an object or array,
   *   depending on the response format.
   */
  public function getBalance($currency = '') {
    $currency = strtoupper($currency);
    $url = 'balance';
    if (!empty($currency)) {
      $url .= '/' . strtoupper($currency);
    }

    $requestResult = $this->sendRequest($url);

    return $this->handleResult($requestResult);
  }

  /**
   * Retrieve a list of emails based on SMTP filters.
   *
   * @param int $limit
   *   The maximum number of emails to return (default is 0, which
   *   means no limit).
   * @param int $offset
   *   The number of emails to skip before starting to return
   *   results (default is 0).
   * @param string $fromDate
   *   The start date for filtering emails (optional).
   * @param string $toDate
   *   The end date for filtering emails (optional).
   * @param string $sender
   *   The sender's email address to filter by (optional).
   * @param string $recipient
   *   The recipient's email address to filter by (optional).
   * @param string $country
   *   The country filter for emails (default is 'off', meaning no
   *   country filtering).
   *
   * @return mixed
   *   The filtered list of emails, typically as
   *   an object or array depending on the response format.
   */
  public function smtpListEmails($limit = 0, $offset = 0, $fromDate = '', $toDate = '', $sender = '', $recipient = '', $country = 'off') {
    $data = [
      'limit' => $limit,
      'offset' => $offset,
      'from' => $fromDate,
      'to' => $toDate,
      'sender' => $sender,
      'recipient' => $recipient,
      'country' => $country,
    ];

    $requestResult = $this->sendRequest('smtp/emails', 'GET', $data);

    return $this->handleResult($requestResult);
  }

  /**
   * Retrieve information about an email by its ID.
   *
   * @param string $id
   *   The ID of the email to retrieve information for.
   *
   * @return \stdClass
   *   The email information as an object.
   */
  public function smtpGetEmailInfoById($id) {
    if (empty($id)) {
      return $this->handleError('Empty id');
    }

    $requestResult = $this->sendRequest('smtp/emails/' . $id);

    return $this->handleResult($requestResult);
  }

  /**
   * Retrieve a list of unsubscribed emails from the SMTP service.
   *
   * @param int|null $limit
   *   The maximum number of unsubscribed emails to return (optional).
   * @param int|null $offset
   *   The number of unsubscribed emails to skip before starting
   *   to return results (optional).
   *
   * @return mixed
   *   The list of unsubscribed emails, typically as an object or
   *   array depending on the response format.
   */
  public function smtpListUnsubscribed($limit = NULL, $offset = NULL) {
    $data = [];
    if (NULL !== $limit) {
      $data['limit'] = $limit;
    }
    if (NULL !== $offset) {
      $data['offset'] = $offset;
    }

    $requestResult = $this->sendRequest('smtp/unsubscribe', 'GET', $data);

    return $this->handleResult($requestResult);
  }

  /**
   * Add one or more emails to the unsubscribe list for SMTP.
   *
   * @param string|array $emails
   *   A single email address or an array of email addresses to unsubscribe.
   *
   * @return \stdClass
   *   The result of the unsubscribe operation as an object.
   */
  public function smtpUnsubscribeEmails($emails) {
    if (empty($emails)) {
      return $this->handleError('Empty emails');
    }

    $data = [
      'emails' => serialize($emails),
    ];

    $requestResult = $this->sendRequest('smtp/unsubscribe', 'POST', $data);

    return $this->handleResult($requestResult);
  }

  /**
   * Remove one or more emails from the SMTP unsubscribe list.
   *
   * @param string|array $emails
   *   A single email address or an array of email addresses to remove
   *   from the unsubscribe list.
   *
   * @return \stdClass
   *   The result of the remove operation as an object.
   */
  public function smtpRemoveFromUnsubscribe($emails) {
    if (empty($emails)) {
      return $this->handleError('Empty emails');
    }

    $data = [
      'emails' => serialize($emails),
    ];

    $requestResult = $this->sendRequest('smtp/unsubscribe', 'DELETE', $data);

    return $this->handleResult($requestResult);
  }

  /**
   * Retrieve a list of IP addresses associated with SMTP.
   *
   * @return mixed
   *   The list of IP addresses, typically as an object or
   *   array depending on the response format.
   */
  public function smtpListIp() {
    $requestResult = $this->sendRequest('smtp/ips');

    return $this->handleResult($requestResult);
  }

  /**
   * Retrieve a list of allowed domains for SMTP.
   *
   * @return mixed
   *   The list of allowed domains, typically as an object or
   *   array depending on the response format.
   */
  public function smtpListAllowedDomains() {
    $requestResult = $this->sendRequest('smtp/domains');

    return $this->handleResult($requestResult);
  }

  /**
   * Add a new domain to the SMTP allowed domains list.
   *
   * @param string $email
   *   The email address associated with the domain to be added.
   *
   * @return \stdClass
   *   The result of the domain addition operation as an object.
   */
  public function smtpAddDomain($email) {
    if (empty($email)) {
      return $this->handleError('Empty email');
    }

    $data = [
      'email' => $email,
    ];

    $requestResult = $this->sendRequest('smtp/domains', 'POST', $data);

    return $this->handleResult($requestResult);
  }

  /**
   * Verify a domain associated with a given email address for SMTP.
   *
   * @param string $email
   *   The email address whose domain is to be verified.
   *
   * @return \stdClass
   *   The result of the domain verification operation as an object.
   */
  public function smtpVerifyDomain($email) {
    if (empty($email)) {
      return $this->handleError('Empty email');
    }

    $requestResult = $this->sendRequest('smtp/domains/' . $email);

    return $this->handleResult($requestResult);
  }

  /**
   * Send an email via SMTP.
   *
   * @param string $email
   *   The email address to which the email will be sent.
   *
   * @return \stdClass
   *   The result of the mail sending operation as an object.
   */
  public function smtpSendMail($email) {
    if (empty($email)) {
      return $this->handleError('Empty email data');
    }

    $emailData = $email;
    if (isset($email['html'])) {
      $emailData['html'] = base64_encode($email['html']);
    }

    $data = [
      'email' => serialize($emailData),
    ];

    $requestResult = $this->sendRequest('smtp/emails', 'POST', $data);

    if ($requestResult->http_code !== 200 && !$this->retry) {
      $this->retry = TRUE;
      sleep(2);

      return $this->smtpSendMail($email);
    }

    return $this->handleResult($requestResult);
  }

  /**
   * Retrieve a list of push campaigns.
   *
   * @param int|null $limit
   *   The maximum number of campaigns to return (optional).
   * @param int|null $offset
   *   The number of campaigns to skip before starting to return
   *   results (optional).
   *
   * @return mixed
   *   The list of push campaigns, typically as an object or
   *   array depending on the response format.
   */
  public function pushListCampaigns($limit = NULL, $offset = NULL) {
    $data = [];
    if (NULL !== $limit) {
      $data['limit'] = $limit;
    }
    if (NULL !== $offset) {
      $data['offset'] = $offset;
    }

    $requestResult = $this->sendRequest('push/tasks', 'GET', $data);

    return $this->handleResult($requestResult);
  }

  /**
   * Retrieve a list of websites for push campaigns.
   *
   * @param int|null $limit
   *   The maximum number of websites to return (optional).
   * @param int|null $offset
   *   The number of websites to skip before starting to return
   *   results (optional).
   *
   * @return mixed
   *   The list of websites, typically as an object or array depending
   *   on the response format.
   */
  public function pushListWebsites($limit = NULL, $offset = NULL) {
    $data = [];
    if (NULL !== $limit) {
      $data['limit'] = $limit;
    }
    if (NULL !== $offset) {
      $data['offset'] = $offset;
    }

    $requestResult = $this->sendRequest('push/websites', 'GET', $data);

    return $this->handleResult($requestResult);
  }

  /**
   * Retrieve the total count of websites for push campaigns.
   *
   * @return mixed
   *   The total number of websites, typically as an object or array
   *   depending on the response format.
   */
  public function pushCountWebsites() {
    $requestResult = $this->sendRequest('push/websites/total');

    return $this->handleResult($requestResult);
  }

  /**
   * Retrieve a list of all variables associated with a specific website.
   *
   * @param string $websiteId
   *   The ID of the website for which to retrieve the variables.
   *
   * @return mixed
   *   The list of variables for the specified website, typically
   *   as an object or array depending on the response format.
   */
  public function pushListWebsiteVariables($websiteId) {
    $requestResult = $this->sendRequest('push/websites/' . $websiteId . '/variables');

    return $this->handleResult($requestResult);
  }

  /**
   * Retrieve a list of subscriptions for a specific website.
   *
   * @param string $websiteID
   *   The ID of the website for which to retrieve the subscriptions.
   * @param int|null $limit
   *   The maximum number of subscriptions to return (optional).
   * @param int|null $offset
   *   The number of subscriptions to skip before starting to return
   *   results (optional).
   *
   * @return mixed
   *   The list of subscriptions for the specified website,
   *   typically as an object or array depending on the response format.
   */
  public function pushListWebsiteSubscriptions($websiteID, $limit = NULL, $offset = NULL) {
    $data = [];
    if (NULL !== $limit) {
      $data['limit'] = $limit;
    }
    if (NULL !== $offset) {
      $data['offset'] = $offset;
    }

    $requestResult = $this->sendRequest('push/websites/' . $websiteID . '/subscriptions', 'GET', $data);

    return $this->handleResult($requestResult);
  }

  /**
   * Retrieve the total count of subscriptions for a specific website.
   *
   * @param string $websiteID
   *   The ID of the website for which to retrieve the subscription count.
   *
   * @return mixed
   *   The total number of subscriptions for the specified website,
   *   typically as an object or array depending on the response format.
   */
  public function pushCountWebsiteSubscriptions($websiteID) {
    $requestResult = $this->sendRequest('push/websites/' . $websiteID . '/subscriptions/total');

    return $this->handleResult($requestResult);
  }

  /**
   * Set the state for a specific subscription.
   *
   * @param string $subscriptionID
   *   The ID of the subscription to update.
   * @param string $stateValue
   *   The new state to set for the subscription.
   *
   * @return mixed
   *   The result of setting the subscription state, typically as an
   *   object or array depending on the response format.
   */
  public function pushSetSubscriptionState($subscriptionID, $stateValue) {
    $data = [
      'id' => $subscriptionID,
      'state' => $stateValue,
    ];

    $requestResult = $this->sendRequest('push/subscriptions/state', 'POST', $data);

    return $this->handleResult($requestResult);
  }

  /**
   * Retrieve common information about a specific website.
   *
   * @param string $websiteId
   *   The ID of the website to retrieve information for.
   *
   * @return mixed
   *   The common website information, typically as an object
   *   or array depending on the response format.
   */
  public function pushGetWebsiteInfo($websiteId) {
    $requestResult = $this->sendRequest('push/websites/info/' . $websiteId);

    return $this->handleResult($requestResult);
  }

  /**
   * Create a new push campaign (task).
   *
   * @param array $taskInfo
   *   Information about the task, such as campaign details.
   * @param array $additionalParams
   *   Optional additional parameters for the task.
   *
   * @return \stdClass
   *   The result of the push task creation, typically as an object.
   */
  public function createPushTask($taskInfo, array $additionalParams = []) {
    $data = $taskInfo;
    if (!isset($data['ttl'])) {
      $data['ttl'] = 0;
    }
    if (empty($data['title']) || empty($data['website_id']) || empty($data['body'])) {
      return $this->handleError('Not all data');
    }
    if ($additionalParams) {
      foreach ($additionalParams as $key => $val) {
        $data[$key] = $val;
      }
    }

    $requestResult = $this->sendRequest('push/tasks', 'POST', $data);

    return $this->handleResult($requestResult);
  }

  /**
   * Retrieve the integration code for Push Notifications.
   *
   * @param string $websiteID
   *   The ID of the website for which to retrieve the integration code.
   *
   * @return \stdClass
   *   The integration code for the specified website, typically as an object.
   */
  public function getPushIntegrationCode($websiteID) {
    if (empty($websiteID)) {
      return $this->handleError('Empty website id');
    }

    $requestResult = $this->sendRequest('push/websites/' . $websiteID . '/code');

    return $this->handleResult($requestResult);
  }

  /**
   * Retrieve statistics for a specific push campaign.
   *
   * @param string $campaignID
   *   The ID of the push campaign for which to retrieve statistics.
   *
   * @return \stdClass
   *   The statistics of the specified push campaign, typically as an object.
   */
  public function getPushCampaignStat($campaignID) {
    $requestResult = $this->sendRequest('push/tasks/' . $campaignID);

    return $this->handleResult($requestResult);
  }

  /**
   * Start an event automation in SendPulse 360.
   *
   * @param string $eventName
   *   The name of the event to trigger.
   * @param array $variables
   *   Variables required to trigger the event, typically
   *   including 'email' or 'phone'.
   *
   * @return \stdClass
   *   The result of starting the event automation, typically as
   *   an object.
   *
   * @throws \Exception
   *   If the event name is empty or both email and phone are missing
   *   from the variables.
   */
  public function startEventAutomation360($eventName, array $variables) {
    if (!$eventName) {
      return $this->handleError('Event name is empty');
    }
    if (!array_key_exists('email', $variables) && !array_key_exists('phone', $variables)) {
      return $this->handleError('Email and phone is empty');
    }

    $requestResult = $this->sendRequest('events/name/' . $eventName, 'POST', $variables);

    return $this->handleResult($requestResult);
  }

  /**
   * Add phone numbers to an address book.
   *
   * @param string $bookID
   *   The ID of the address book to which the phone numbers will
   *   be added.
   * @param array $phones
   *   An array of phone numbers to be added to the address book.
   *
   * @return \stdClass
   *   The result of adding the phone numbers to the address book,
   *   typically as an object.
   *
   * @throws \Exception
   *   If the book ID is empty.
   */
  public function addPhones($bookID, array $phones) {
    if (empty($bookID)) {
      return $this->handleError('Empty book id');
    }

    $data = [
      'addressBookId' => $bookID,
      'phones' => json_encode($phones),
    ];

    $requestResult = $this->sendRequest('sms/numbers', 'POST', $data);

    return $this->handleResult($requestResult);
  }

  /**
   * Add phone numbers with associated variables to an address book.
   *
   * @param string $bookID
   *   The ID of the address book to which the phone numbers with
   *   variables will be added.
   * @param array $phonesWithVariables
   *   An array of phone numbers along with associated variables
   *   (e.g., custom fields) to be added to the address book.
   *
   * @return \stdClass
   *   The result of adding the phone numbers with variables to
   *   the address book, typically as an object.
   *
   * @throws \Exception
   *   If the book ID is empty.
   */
  public function addPhonesWithVariables($bookID, array $phonesWithVariables) {
    if (empty($bookID)) {
      return $this->handleError('Empty book id');
    }

    $data = [
      'addressBookId' => $bookID,
      'phones' => json_encode($phonesWithVariables),
    ];

    $requestResult = $this->sendRequest('sms/numbers/variables', 'POST', $data);

    return $this->handleResult($requestResult);
  }

  /**
   * Update variables for phone numbers in an address book.
   *
   * @param string $bookID
   *   The ID of the address book to update.
   * @param array $phones
   *   An array of phone numbers that need their variables updated.
   * @param array $variables
   *   An array of new variables to associate with the given phone numbers.
   *
   * @return \stdClass
   *   The result of the update operation, typically as an object
   *   containing success or error information.
   */
  public function updatePhoneVaribales($bookID, array $phones, array $variables) {
    if (empty($bookID)) {
      return $this->handleError('Empty book id');
    }

    $data = [
      'addressBookId' => $bookID,
      'phones' => json_encode($phones),
      'variables' => json_encode($variables),
    ];

    $requestResult = $this->sendRequest('sms/numbers', 'PUT', $data);

    return $this->handleResult($requestResult);
  }

  /**
   * Delete phone numbers from an address book.
   *
   * @param string $bookID
   *   The ID of the address book from which the phone numbers
   *   should be deleted.
   * @param array $phones
   *   An array of phone numbers to be deleted from the address book.
   *
   * @return \stdClass
   *   The result of the delete operation, typically containing
   *   success or error information.
   *
   * @throws \Exception
   *   If the book ID is empty or the request fails.
   */
  public function deletePhones($bookID, array $phones) {
    if (empty($bookID)) {
      return $this->handleError('Empty book id');
    }

    $data = [
      'addressBookId' => $bookID,
      'phones' => json_encode($phones),
    ];

    $requestResult = $this->sendRequest('sms/numbers', 'DELETE', $data);

    return $this->handleResult($requestResult);
  }

  /**
   * Retrieve information about a specific phone number from the address book.
   *
   * @param string $bookID
   *   The ID of the address book containing the phone number.
   * @param string $phoneNumber
   *   The phone number for which information is being requested.
   *
   * @return \stdClass
   *   The result of the request, typically containing phone number details.
   *
   * @throws \Exception
   *   If the book ID is empty or the request fails.
   */
  public function getPhoneInfo($bookID, $phoneNumber) {
    if (empty($bookID)) {
      return $this->handleError('Empty book id');
    }

    $requestResult = $this->sendRequest('sms/numbers/info/' . $bookID . '/' . $phoneNumber);

    return $this->handleResult($requestResult);
  }

  /**
   * Add phone numbers to the blacklist.
   *
   * @param array $phones
   *   The list of phone numbers to be added to the blacklist.
   *   The phone numbers should be in an array format.
   *
   * @return \stdClass
   *   The result of the request, typically containing the success status.
   *
   * @throws \Exception
   *   If the request fails or if the input is invalid.
   */
  public function addPhonesToBlacklist(array $phones) {
    $data = [
      'phones' => json_encode($phones),
    ];

    $requestResult = $this->sendRequest('sms/black_list', 'POST', $data);

    return $this->handleResult($requestResult);
  }

  /**
   * Remove phone numbers from the blacklist.
   *
   * @param array $phones
   *   The list of phone numbers to be removed from the blacklist.
   *   The phone numbers should be provided in an array format.
   *
   * @return \stdClass
   *   The result of the request, typically containing the success status.
   *
   * @throws \Exception
   *   If the request fails or if the input is invalid.
   */
  public function removePhonesFromBlacklist(array $phones) {
    $data = [
      'phones' => json_encode($phones),
    ];

    $requestResult = $this->sendRequest('sms/black_list', 'DELETE', $data);

    return $this->handleResult($requestResult);
  }

  /**
   * Retrieve the list of phone numbers from the blacklist.
   *
   * @return \stdClass
   *   The result of the request, typically containing the list of
   *   blacklisted phone numbers.
   *
   * @throws \Exception
   *   If the request fails or there is an issue fetching the data.
   */
  public function getPhonesFromBlacklist() {
    $requestResult = $this->sendRequest('sms/black_list');

    return $this->handleResult($requestResult);
  }

  /**
   * Create and send an SMS campaign.
   *
   * Based on phones in the specified address book.
   *
   * @param int $bookID
   *   The ID of the address book to send SMS to.
   * @param array $params
   *   Parameters for the SMS campaign (e.g., message, sender).
   * @param array $additionalParams
   *   Optional additional parameters for the SMS campaign.
   *
   * @return \stdClass
   *   The result of the SMS campaign creation request.
   *
   * @throws \Exception
   *   If there is an issue with the request or missing parameters.
   */
  public function sendSmsByBook($bookID, array $params, array $additionalParams = []) {
    if (empty($bookID)) {
      return $this->handleError('Empty book id');
    }

    $data = [
      'addressBookId' => $bookID,
    ];

    $data = array_merge($data, $params);

    if ($additionalParams) {
      $data = array_merge($data, $additionalParams);
    }

    $requestResult = $this->sendRequest('sms/campaigns', 'POST', $data);

    return $this->handleResult($requestResult);
  }

  /**
   * Create and send an SMS campaign based on a list of phone numbers.
   *
   * @param array $phones
   *   List of phone numbers to send SMS to.
   * @param array $params
   *   Parameters for the SMS campaign (e.g., message, sender).
   * @param array $additionalParams
   *   Optional additional parameters for the SMS campaign.
   *
   * @return \stdClass
   *   The result of the SMS campaign creation request.
   *
   * @throws \Exception
   *   If there is an issue with the request or missing parameters.
   */
  public function sendSmsByList(array $phones, array $params, array $additionalParams) {
    $data = [
      'phones' => json_encode($phones),
    ];

    $data = array_merge($data, $params);

    if ($additionalParams) {
      $data = array_merge($data, $additionalParams);
    }

    $requestResult = $this->sendRequest('sms/send', 'POST', $data);

    return $this->handleResult($requestResult);
  }

  /**
   * List SMS campaigns.
   *
   * @param array|null $params
   *   Optional parameters for filtering or pagination of SMS campaigns.
   *
   * @return \stdClass
   *   The result of the request, typically containing a list of SMS campaigns.
   */
  public function listSmsCampaigns(?array $params = NULL) {
    $requestResult = $this->sendRequest('sms/campaigns/list', 'GET', $params);

    return $this->handleResult($requestResult);
  }

  /**
   * Get information about an SMS campaign.
   *
   * @param string $campaignID
   *   The ID of the SMS campaign.
   *
   * @return \stdClass
   *   The result of the request, typically containing information about
   *   the SMS campaign.
   */
  public function getSmsCampaignInfo($campaignID) {
    $requestResult = $this->sendRequest('sms/campaigns/info/' . $campaignID);

    return $this->handleResult($requestResult);
  }

  /**
   * Cancels an ongoing SMS campaign.
   *
   * This function sends a request to cancel an SMS campaign using the
   * given campaign ID.
   * The request is made using the HTTP PUT method.
   *
   * @param string|int $campaignID
   *   The unique identifier of the SMS campaign to be canceled.
   *
   * @return \stdClass
   *   The response object containing the result of the cancellation request.
   */
  public function cancelSmsCampaign($campaignID) {
    $requestResult = $this->sendRequest('sms/campaigns/cancel/' . $campaignID, 'PUT');

    return $this->handleResult($requestResult);
  }

  /**
   * Retrieves the cost of an SMS campaign.
   *
   * Based on an address book or a list of phone numbers.
   *
   * This function calculates the estimated cost of sending an SMS campaign.
   * The campaign cost can be determined using either an address book
   * ID or a list of phone numbers.
   * If additional parameters are provided, they will be merged with
   * the main parameters.
   *
   * @param array $params
   *   An associative array containing campaign details, including:
   *   - 'addressBookId' (int|null): The ID of the address book.
   *   - 'phones' (array|null): A list of phone numbers.
   * @param array|null $additionalParams
   *   Optional additional parameters to be merged with $params.
   *
   * @return \stdClass
   *   The response object containing the calculated cost or an error message.
   */
  public function getSmsCampaignCost(array $params, ?array $additionalParams = NULL) {
    if (!isset($params['addressBookId']) && !isset($params['phones'])) {
      return $this->handleError('You mast pass phones list or addressbook ID');
    }

    if ($additionalParams) {
      $params = array_merge($params, $additionalParams);
    }

    $requestResult = $this->sendRequest('sms/campaigns/cost', 'GET', $params);

    return $this->handleResult($requestResult);
  }

  /**
   * Deletes an SMS campaign.
   *
   * This function sends a request to delete an existing SMS campaign
   * using the given campaign ID.
   * The request is made using the HTTP DELETE method.
   *
   * @param string|int $campaignID
   *   The unique identifier of the SMS campaign to be deleted.
   *
   * @return \stdClass
   *   The response object containing the result of the deletion request.
   */
  public function deleteSmsCampaign($campaignID) {
    $requestResult = $this->sendRequest('sms/campaigns', 'DELETE', ['id' => $campaignID]);

    return $this->handleResult($requestResult);
  }

}
