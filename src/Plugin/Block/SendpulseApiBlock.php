<?php

namespace Drupal\sendpulse_api\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\sendpulse_api\Service\SendpulseApi;

/**
 * Provides a sendpulse api signup form per list that is enabled.
 *
 * @Block(
 *   id = "sendpulse_api",
 *   admin_label = @Translation("Sendpulse Api Signup Form"),
 *   deriver = "Drupal\sendpulse_api\Plugin\Derivative\SendpulseApiBlockDerivative"
 * )
 */
class SendpulseApiBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Drupal\Core\Form\FormBuilderInterface.
   *
   * @var \Drupal\Core\Form\FormBuilderInterface
   */
  protected $formBuilder;

  /**
   * Drupal\sendpulse_api\Service\SendpulseApi.
   *
   * @var \Drupal\sendpulse_api\Service\SendpulseApi
   *   Sendpulse ems service.
   */
  protected $sendpulseApi;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, FormBuilderInterface $form_builder, SendpulseApi $sendpulseApi) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->formBuilder = $form_builder;
    $this->sendpulseApi = $sendpulseApi;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('form_builder'),
      $container->get('sendpulse_api')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);
    $config = $this->getConfiguration();
    $ccConfig = $this->sendpulseApi->getConfig();
    $customFields = $this->sendpulseApi->getCustomFields();

    $form['cc_fields'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Add Form Fields from Sendpulse Api'),
      '#description' => $this->t('Select any <a href="https://sendpulse.com/integrations/api" target="_blank" rel="nofollow noreferrer">available fields from Sendpulse Api</a> to add to the signup form.'),
    ];

    foreach ($ccConfig['fields'] as $fieldName => $fieldLabel) {
      $form['cc_fields']['field_' . $fieldName] = [
        '#type' => 'fieldset',
        '#title' => $this->t('@label', ['@label' => $fieldLabel]),
      ];

      $form['cc_fields']['field_' . $fieldName][$fieldName] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Display @field_label field?', ['@field_label' => $fieldLabel]),
        '#default_value' => $config[$fieldName] ?? 0,
      ];
      $form['cc_fields']['field_' . $fieldName][$fieldName . '_required'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Require @field_label field?', ['@field_label' => $fieldLabel]),
        '#default_value' => $config[$fieldName . '_required'] ?? 0,
      ];
    }

    // Custom Fields.
    if ($customFields && $customFields->custom_fields && is_array($customFields->custom_fields) && count($customFields->custom_fields) > 0) {
      $form['custom_fields'] = [
        '#type' => 'fieldset',
        '#title' => $this->t('Custom Fields'),
        '#description' => $this->t('Select any <a href="https://knowledgebase.sendpulseapi.com/articles/KnowledgeBase/33120-Create-and-Manage-Custom-Contact-Fields?lang=en_US" target="_blank" rel="nofollow noreferrer">custom fields from your Sendpulse Api account</a> to add to the signup form.'),
        '#tree' => TRUE,
      ];

      foreach ($customFields->custom_fields as $field) {
        $attributes = [];
        $field_required = FALSE;

        if (isset($field->is_required) && $field->is_required) {
          $field_required = TRUE;
          $attributes = ['disabled' => 'disabled'];

        }
        elseif (isset($config['custom_fields'][$field->custom_field_id]['required'])) {
          $field_required = $config['custom_fields'][$field->custom_field_id]['required'];
        }

        $form['custom_fields'][$field->custom_field_id] = [
          '#type' => 'fieldset',
          '#title' => $this->t('@field_label', ['@field_label' => $field->label]),
        ];
        $form['custom_fields'][$field->custom_field_id]['display'] = [
          '#type' => 'checkbox',
          '#title' => $this->t('Display @field_label field', ['@field_label' => $field->label]),
          '#default_value' => $config['custom_fields'][$field->custom_field_id]['display'] ?? NULL,
        ];
        $form['custom_fields'][$field->custom_field_id]['required'] = [
          '#type' => 'checkbox',
          '#title' => $this->t('Require @field_label field', ['@field_label' => $field->label]),
          '#default_value' => $field_required,
          '#attributes' => $attributes,
        ];

        $form['custom_fields'][$field->custom_field_id]['name'] = [
          '#type' => 'hidden',
          '#value' => $field->name,
        ];
        $form['custom_fields'][$field->custom_field_id]['type'] = [
          '#type' => 'hidden',
          '#value' => $field->type,
        ];
        $form['custom_fields'][$field->custom_field_id]['label'] = [
          '#type' => 'hidden',
          '#value' => $field->label,
        ];

        if ($field->type === 'date') {
          $form['custom_fields'][$field->custom_field_id]['#description'] = $this->t('Requires the <a href="https://www.drupal.org/docs/8/core/modules/datetime" target="_blank" rel="nofollow noreferrer">datetime</a> module to be installed.');
        }
      }
    }

    $form['body'] = [
      '#type' => 'text_format',
      '#title' => $this->t('Body'),
      '#default_value' => isset($config['body']) ? $config['body']['value'] : NULL,
      '#format' => isset($config['format']) ? $config['body']['format'] : NULL,
    ];
    $form['success_message'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Custom Success Message'),
      '#default_value' => $config['success_message'] ?? NULL,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    parent::blockSubmit($form, $form_state);

    $values = $form_state->getValues();
    $ccConfig = $this->sendpulseApi->getConfig();

    foreach ($ccConfig['fields'] as $fieldName => $fieldLabel) {
      if (isset($values['cc_fields']['field_' . $fieldName][$fieldName])) {
        $this->configuration[$fieldName] = $values['cc_fields']['field_' . $fieldName][$fieldName];
      }

      if (isset($values['cc_fields']['field_' . $fieldName][$fieldName . '_required'])) {
        $this->configuration[$fieldName . '_required'] = $values['cc_fields']['field_' . $fieldName][$fieldName . '_required'];
      }
    }

    $this->configuration['custom_fields'] = $values['custom_fields'];
    $this->configuration['body'] = $values['body'];
    $this->configuration['success_message'] = $values['success_message'];
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $listConfig = $this->getConfiguration();
    $listConfig['list_id'] = str_replace('sendpulse_api:', '', $this->getPluginId());

    return $this->formBuilder->getForm('Drupal\sendpulse_api\Form\SendpulseApiBlockForm', $listConfig);
  }

}
