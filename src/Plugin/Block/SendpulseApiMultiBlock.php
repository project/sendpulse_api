<?php

namespace Drupal\sendpulse_api\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\sendpulse_api\Service\SendpulseApi;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * Provides a sendpulse api signup form per list that is enabled.
 *
 * @Block(
 *   id = "sendpulse_api_multi",
 *   admin_label = @Translation("Sendpulse Api Multiple Lists Signup Form"),
 * )
 */
class SendpulseApiMultiBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Drupal\Core\Form\FormBuilderInterface.
   *
   * @var \Drupal\Core\Form\FormBuilderInterface
   */
  protected $formBuilder;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, FormBuilderInterface $form_builder, SendpulseApi $sendpulse_api, ConfigFactoryInterface $config) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->formBuilder = $form_builder;
    $this->sendpulseApi = $sendpulse_api;
    $this->config = $config;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('form_builder'),
      $container->get('sendpulse_api'),
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);
    $config = $this->getConfiguration();
    $ccConfig = $this->sendpulseApi->getConfig();
    $customFields = $this->sendpulseApi->getCustomFields();
    $enabled = $this->config->get('sendpulse_api.enabled_lists')->getRawData();

    // Custom Fields.
    if ($customFields && $customFields->custom_fields && is_array($customFields->custom_fields) && count($customFields->custom_fields) > 0) {
      $form['custom_fields'] = [
        '#type' => 'fieldset',
        '#title' => $this->t('Custom Fields'),
        '#description' => $this->t('Select any <a href="https://knowledgebase.sendpulseapi.com/articles/KnowledgeBase/33120-Create-and-Manage-Custom-Contact-Fields?lang=en_US" target="_blank" rel="nofollow noreferrer">custom fields from your Sendpulse Api account</a> to add to the signup form.'),
        '#tree' => TRUE,
      ];

      foreach ($customFields->custom_fields as $field) {
        $form['custom_fields'][$field->custom_field_id] = [
          '#type' => 'fieldset',
          '#title' => $this->t('@field_label', ['@field_label' => $field->label]),
        ];
        $form['custom_fields'][$field->custom_field_id]['display'] = [
          '#type' => 'checkbox',
          '#title' => $this->t('Display @field_label field', ['@field_label' => $field->label]),
          '#default_value' => $config['custom_fields'][$field->custom_field_id]['display'] ?? NULL,
        ];
        $form['custom_fields'][$field->custom_field_id]['required'] = [
          '#type' => 'checkbox',
          '#title' => $this->t('Require @field_label field', ['@field_label' => $field->label]),
          '#default_value' => $config['custom_fields'][$field->custom_field_id]['required'] ?? NULL,
        ];
        $form['custom_fields'][$field->custom_field_id]['name'] = [
          '#type' => 'hidden',
          '#value' => $field->label ?? '',
        ];
        $form['custom_fields'][$field->custom_field_id]['type'] = [
          '#type' => 'hidden',
          '#value' => $field->type ?? '',
        ];
        $form['custom_fields'][$field->custom_field_id]['label'] = [
          '#type' => 'hidden',
          '#value' => $field->label ?? '',
        ];

        if ($field->type === 'date') {
          $form['custom_fields'][$field->custom_field_id]['#description'] = $this->t('Requires the <a href="https://www.drupal.org/docs/8/core/modules/datetime" target="_blank" rel="nofollow noreferrer">datetime</a> module to be installed.');
        }
      }
    }

    $form['body'] = [
      '#type' => 'text_format',
      '#title' => $this->t('Body'),
      '#default_value' => isset($config['body']) ? $config['body']['value'] : NULL,
      '#format' => isset($config['format']) ? $config['body']['format'] : NULL,
    ];
    $form['success_message'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Custom Success Message'),
      '#default_value' => $config['success_message'] ?? NULL,
    ];

    $form['lists'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Your Sendpulse Api Lists'),
      '#description' => $this->t('Check the lists you would like to enable in this block. Note that if a list is missing, make sure it is enabled <a href="/admin/config/services/sendpulse-api/lists" target="_blank">here</a>.'),
      '#default_value' => $config['lists'] ?? NULL,
    ];

    if (isset($ccConfig['api_secret'])) {
      $lists = $this->sendpulseApi->getMailingLists();

      if ($lists && is_array($lists) && count($lists) > 0) {
        foreach ($lists as $list_id => $list) {
          if (isset($enabled[$list_id]) && $enabled[$list_id] === 1) {
            $form['lists']['#options'][$list_id] = $list->name;
          }
        }
      }
    }
    else {
      $form['lists']['#description'] = $this->t('You must authorize Sendpulse Api before enabling a list.');
    }

    $form['lists_select_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label for list checkboxes'),
      '#default_value' => $config['lists_select_label'] ?? 'Sign me up for:',
    ];

    $form['lists_user_select'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Allow user to choose which lists from above?'),
      '#default_value' => $config['lists_user_select'] ?? 0,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    parent::blockSubmit($form, $form_state);
    $values = $form_state->getValues();
    $this->configuration['custom_fields'] = $values['custom_fields'];
    $this->configuration['body'] = $values['body'];
    $this->configuration['success_message'] = $values['success_message'];
    $this->configuration['lists'] = $values['lists'];
    $this->configuration['lists_user_select'] = $values['lists_user_select'];
    $this->configuration['lists_select_label'] = $values['lists_select_label'];
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $listConfig = $this->getConfiguration();
    $listConfig['list_id'] = str_replace('sendpulse_api:', '', $this->getPluginId());
    $listConfig['lists_all'] = $this->sendpulseApi->getMailingLists();

    return $this->formBuilder->getForm('Drupal\sendpulse_api\Form\SendpulseApiBlockForm', $listConfig);
  }

}
