<?php

namespace Drupal\sendpulse_api\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Drupal\sendpulse_api\Service\SendpulseApi;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides block plugin definitions for Sendpulse Api blocks.
 *
 * @see \Drupal\sendpulse_api\Plugin\Block\SendpulseApiBlock
 */
class SendpulseApiBlockDerivative extends DeriverBase implements ContainerDeriverInterface {

  /**
   * Drupal\Core\Config\ConfigFactoryInterface.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Drupal\sendpulse_api\Service\SendpulseApi.
   *
   * @var \Drupal\sendpulse_api\Service\SendpulseApi
   *   Sendpulse ems service.
   */
  protected $sendpulseApi;

  /**
   * SendpulseApiBlockDerivative constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   ConfigFactoryInterface.
   * @param \Drupal\sendpulse_api\Service\SendpulseApi $sendpulseApi
   *   Sendpulse ems service.
   */
  public function __construct(ConfigFactoryInterface $configFactory, SendpulseApi $sendpulseApi) {
    $this->configFactory = $configFactory;
    $this->sendpulseApi = $sendpulseApi;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $base_plugin_id) {
    return new static(
      $container->get('config.factory'),
      $container->get('sendpulse_api')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    $lists = $this->sendpulseApi->getEnabledMailingLists(FALSE);

    if ($lists) {
      foreach ($lists as $id => $value) {
        if ($value->enabled === TRUE) {
          $this->derivatives[$id] = $base_plugin_definition;
          $this->derivatives[$id]['admin_label'] = $lists[$id]->name . '  Signup Block';
        }
      }
    }

    return $this->derivatives;
  }

}
