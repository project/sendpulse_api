<?php

namespace Drupal\sendpulse_api\Plugin;

/**
 * Interface for the SendPulse API.
 */
interface ApiInterface {

  /**
   * Create a new address book.
   *
   * @param string $bookName
   *   The name of the address book to be created.
   */
  public function createAddressBook($bookName);

  /**
   * Edit address book name.
   *
   * @param int $id
   *   The ID of the address book to be edited.
   * @param string $newName
   *   The new name of the address book.
   */
  public function editAddressBook($id, $newName);

  /**
   * Remove address book.
   *
   * @param int $id
   *   The ID of the address book to be removed.
   */
  public function removeAddressBook($id);

  /**
   * Get list of address books.
   *
   * @param int|null $limit
   *   The maximum number of address books to retrieve (optional).
   * @param int|null $offset
   *   The offset for pagination (optional).
   */
  public function listAddressBooks($limit = NULL, $offset = NULL);

  /**
   * Get book info.
   *
   * @param int $id
   *   The ID of the address book.
   */
  public function getBookInfo($id);

  /**
   * Get book variables.
   *
   * @param int $id
   *   The ID of the address book.
   */
  public function getBookVariables($id);

  /**
   * Get list of emails from book.
   *
   * @param int $id
   *   The ID of the address book.
   */
  public function getEmailsFromBook($id);

  /**
   * Add new emails to book.
   *
   * @param int $bookID
   *   The ID of the address book.
   * @param array $emails
   *   The list of emails to add.
   */
  public function addEmails($bookID, $emails);

  /**
   * Remove emails from book.
   *
   * @param int $bookID
   *   The ID of the address book.
   * @param array $emails
   *   The list of emails to remove.
   */
  public function removeEmails($bookID, $emails);

  /**
   * Get information about an email from book.
   *
   * @param int $bookID
   *   The ID of the address book.
   * @param string $email
   *   The email address to retrieve information for.
   */
  public function getEmailInfo($bookID, $email);

  /**
   * Calculate the cost of the campaign based on address book.
   *
   * @param int $bookID
   *   The ID of the address book.
   */
  public function campaignCost($bookID);

  /**
   * Get list of campaigns.
   *
   * @param int|null $limit
   *   The maximum number of campaigns to retrieve (optional).
   * @param int|null $offset
   *   The offset for pagination (optional).
   */
  public function listCampaigns($limit = NULL, $offset = NULL);

  /**
   * Get information about a campaign.
   *
   * @param int $id
   *   The ID of the campaign.
   */
  public function getCampaignInfo($id);

  /**
   * Get campaign statistics by countries.
   *
   * @param int $id
   *   The ID of the campaign.
   */
  public function campaignStatByCountries($id);

  /**
   * Get campaign statistics by referrals.
   *
   * @param int $id
   *   The ID of the campaign.
   */
  public function campaignStatByReferrals($id);

  /**
   * Create a new campaign.
   *
   * @param string $senderName
   *   The sender's name.
   * @param string $senderEmail
   *   The sender's email address.
   * @param string $subject
   *   The subject of the campaign.
   * @param string $body
   *   The body content of the campaign.
   * @param int $bookId
   *   The ID of the address book for the campaign.
   * @param string|null $name
   *   The name of the campaign (optional).
   * @param array|null $attachments
   *   Any attachments for the campaign (optional).
   * @param string|null $type
   *   The type of campaign (optional).
   */
  public function createCampaign(
    $senderName,
    $senderEmail,
    $subject,
    $body,
    $bookId,
    $name = NULL,
    $attachments = NULL,
    $type = NULL,
  );

  /**
   * Cancel campaign.
   *
   * @param int $id
   *   The ID of the campaign to be canceled.
   */
  public function cancelCampaign($id);

  /**
   * Get list of allowed senders.
   */
  public function listSenders();

  /**
   * Add a new sender.
   *
   * @param string $senderName
   *   The sender's name.
   * @param string $senderEmail
   *   The sender's email address.
   */
  public function addSender($senderName, $senderEmail);

  /**
   * Remove sender.
   *
   * @param string $email
   *   The email address of the sender to be removed.
   */
  public function removeSender($email);

  /**
   * Activate sender using the code from the mail.
   *
   * @param string $email
   *   The email address of the sender.
   * @param string $code
   *   The activation code sent to the email.
   */
  public function activateSender($email, $code);

  /**
   * Send an email with an activation code to the sender's email.
   *
   * @param string $email
   *   The email address to send the activation code to.
   */
  public function getSenderActivationMail($email);

  /**
   * Get global information about an email.
   *
   * @param string $email
   *   The email address to get information for.
   */
  public function getEmailGlobalInfo($email);

  /**
   * Remove email address from all books.
   *
   * @param string $email
   *   The email address to remove from all address books.
   */
  public function removeEmailFromAllBooks($email);

  /**
   * Get statistics for an email from all campaigns.
   *
   * @param string $email
   *   The email address to get statistics for.
   */
  public function emailStatByCampaigns($email);

  /**
   * Show emails from the blacklist.
   */
  public function getBlackList();

  /**
   * Add an email address to the blacklist.
   *
   * @param array|string $emails
   *   The email(s) to add to the blacklist.
   * @param string|null $comment
   *   An optional comment for the blacklisted email(s).
   */
  public function addToBlackList($emails, $comment = NULL);

  /**
   * Remove an email address from the blacklist.
   *
   * @param array|string $emails
   *   The email(s) to remove from the blacklist.
   */
  public function removeFromBlackList($emails);

  /**
   * Return user balance.
   *
   * @param string $currency
   *   The currency code for the balance (optional).
   */
  public function getBalance($currency = '');

  /**
   * Get a list of emails sent by SMTP.
   *
   * @param int $limit
   *   The maximum number of emails to retrieve.
   * @param int $offset
   *   The offset for pagination.
   * @param string $fromDate
   *   The start date for the emails to retrieve.
   * @param string $toDate
   *   The end date for the emails to retrieve.
   * @param string $sender
   *   The email address of the sender.
   * @param string $recipient
   *   The email address of the recipient.
   * @param string $country
   *   The country to filter the emails by (default is 'off').
   */
  public function smtpListEmails(
    $limit = 0,
    $offset = 0,
    $fromDate = '',
    $toDate = '',
    $sender = '',
    $recipient = '',
    $country = 'off',
  );

  /**
   * Get information about email by its ID.
   *
   * @param int $id
   *   The ID of the email.
   */
  public function smtpGetEmailInfoById($id);

  /**
   * Get list of unsubscribed emails.
   *
   * @param int|null $limit
   *   The maximum number of unsubscribed emails to retrieve (optional).
   * @param int|null $offset
   *   The offset for pagination (optional).
   */
  public function smtpListUnsubscribed($limit = NULL, $offset = NULL);

  /**
   * Unsubscribe emails using SMTP.
   *
   * @param array|string $emails
   *   The email(s) to unsubscribe.
   */
  public function smtpUnsubscribeEmails($emails);

  /**
   * Remove emails from unsubscribe list using SMTP.
   *
   * @param array|string $emails
   *   The email(s) to remove from the unsubscribe list.
   */
  public function smtpRemoveFromUnsubscribe($emails);

  /**
   * Get list of allowed IPs using SMTP.
   */
  public function smtpListIp();

  /**
   * Get list of allowed domains using SMTP.
   */
  public function smtpListAllowedDomains();

  /**
   * Add domain using SMTP.
   *
   * @param string $email
   *   The email address for the domain to be added.
   */
  public function smtpAddDomain($email);

  /**
   * Send confirmation mail to verify new domain.
   *
   * @param string $email
   *   The email address to send the confirmation to.
   */
  public function smtpVerifyDomain($email);

  /**
   * Send mail using SMTP.
   *
   * @param string $email
   *   The email address to send the mail to.
   */
  public function smtpSendMail($email);

  /**
   * Get list of all push campaigns.
   *
   * @param int|null $limit
   *   The maximum number of push campaigns to retrieve (optional).
   * @param int|null $offset
   *   The offset for pagination (optional).
   */
  public function pushListCampaigns($limit = NULL, $offset = NULL);

  /**
   * Get list of websites.
   *
   * @param int|null $limit
   *   The maximum number of websites to retrieve (optional).
   * @param int|null $offset
   *   The offset for pagination (optional).
   */
  public function pushListWebsites($limit = NULL, $offset = NULL);

  /**
   * Get amount of websites.
   */
  public function pushCountWebsites();

  /**
   * Get list of all variables for the website.
   *
   * @param int $websiteID
   *   The ID of the website.
   */
  public function pushListWebsiteVariables($websiteID);

  /**
   * Get list of all subscriptions for the website.
   *
   * @param int $websiteID
   *   The ID of the website.
   * @param int|null $limit
   *   The maximum number of subscriptions to retrieve (optional).
   * @param int|null $offset
   *   The offset for pagination (optional).
   *
   * @return mixed
   *   A list or collection of subscriptions for the specified website.
   */
  public function pushListWebsiteSubscriptions($websiteID, $limit = NULL, $offset = NULL);

  /**
   * Get amount of subscriptions for the site.
   *
   * @param int $websiteID
   *   The ID of the website.
   */
  public function pushCountWebsiteSubscriptions($websiteID);

  /**
   * Set state for subscription.
   *
   * @param int $subscriptionID
   *   The ID of the subscription.
   * @param string $stateValue
   *   The new state value for the subscription.
   */
  public function pushSetSubscriptionState($subscriptionID, $stateValue);

  /**
   * Create new push campaign.
   *
   * @param array $taskInfo
   *   The information for the push campaign.
   * @param array $additionalParams
   *   Additional parameters for the campaign (optional).
   */
  public function createPushTask($taskInfo, array $additionalParams = []);

  /**
   * Get integration code for Push Notifications.
   *
   * @param int $websiteID
   *   The ID of the website.
   */
  public function getPushIntegrationCode($websiteID);

  /**
   * Start event automation.
   *
   * @param string $eventName
   *   The name of the event.
   * @param array $variables
   *   The variables related to the event.
   *
   * @return \stdClass
   *   The result of the event automation.
   */
  public function startEventAutomation360($eventName, array $variables);

  /**
   * Add phones to address book.
   *
   * @param int $bookID
   *   The ID of the address book.
   * @param array $phones
   *   The list of phone numbers to add.
   */
  public function addPhones($bookID, array $phones);

  /**
   * Add phones with variables to address book.
   *
   * @param int $bookID
   *   The ID of the address book.
   * @param array $phones
   *   The list of phone numbers to add.
   */
  public function addPhonesWithVariables($bookID, array $phones);

  /**
   * Update phone variables.
   *
   * @param int $bookID
   *   The ID of the address book.
   * @param array $phones
   *   The list of phone numbers to update.
   * @param array $variables
   *   The list of variables for the phones.
   */
  public function updatePhoneVaribales($bookID, array $phones, array $variables);

  /**
   * Delete phones from book.
   *
   * @param int $bookID
   *   The ID of the address book.
   * @param array $phones
   *   The list of phone numbers to delete.
   */
  public function deletePhones($bookID, array $phones);

  /**
   * Get information about a phone.
   *
   * @param int $bookID
   *   The ID of the address book.
   * @param string $phoneNumber
   *   The phone number to retrieve information for.
   *
   * @return mixed
   *   Get phone info
   */
  public function getPhoneInfo($bookID, $phoneNumber);

  /**
   * Add phones to blacklist.
   *
   * @param array $phones
   *   The list of phones to add to the blacklist.
   */
  public function addPhonesToBlacklist(array $phones);

  /**
   * Remove phones from blacklist.
   *
   * @param array $phones
   *   The list of phones to remove from the blacklist.
   */
  public function removePhonesFromBlacklist(array $phones);

  /**
   * Get list of phones from blacklist.
   *
   * @return mixed
   *   Get phots from blacklist
   */
  public function getPhonesFromBlacklist();

  /**
   * Create SMS campaign based on phones in the address book.
   *
   * @param int $bookId
   *   The ID of the address book.
   * @param array $params
   *   The parameters for the SMS campaign.
   * @param array $additionalParams
   *   Additional parameters for the campaign (optional).
   */
  public function sendSmsByBook($bookId, array $params, array $additionalParams);

  /**
   * Create SMS campaign based on a list of phones.
   *
   * @param array $phones
   *   The list of phone numbers.
   * @param array $params
   *   The parameters for the SMS campaign.
   * @param array $additionalParams
   *   Additional parameters for the campaign (optional).
   */
  public function sendSmsByList(array $phones, array $params, array $additionalParams);

  /**
   * List SMS campaigns.
   *
   * @param array $params
   *   The parameters to filter the list of campaigns.
   *
   * @return mixed
   *   List sms campaigns
   */
  public function listSmsCampaigns(array $params);

  /**
   * Get information about an SMS campaign.
   *
   * @param int $campaignID
   *   The ID of the SMS campaign.
   *
   * @return mixed
   *   Get sms campaign info
   */
  public function getSmsCampaignInfo($campaignID);

  /**
   * Cancel an SMS campaign.
   *
   * @param int $campaignID
   *   The ID of the SMS campaign to cancel.
   */
  public function cancelSmsCampaign($campaignID);

  /**
   * Get SMS campaign cost based on address book or list.
   *
   * @param array $params
   *   The parameters for the campaign.
   * @param array $additionalParams
   *   Additional parameters for the cost calculation.
   *
   * @return mixed
   *   Get sms campaign cost
   */
  public function getSmsCampaignCost(array $params, array $additionalParams);

  /**
   * Delete an SMS campaign.
   *
   * @param int $campaignID
   *   The ID of the SMS campaign to delete.
   */
  public function deleteSmsCampaign($campaignID);

}
