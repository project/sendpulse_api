<?php

namespace Drupal\sendpulse_api\Plugin\Field\FieldFormatter;

use Drupal\options\Plugin\Field\FieldFormatter\OptionsDefaultFormatter;

/**
 * Plugin implementation of the 'sendpulse_api_lists_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "sendpulse_api_lists_formatter",
 *   label = @Translation("Default"),
 *   field_types = {
 *     "sendpulse_api_lists",
 *   }
 * )
 */
class SendpulseApiListDefaultFormatter extends OptionsDefaultFormatter {
}
