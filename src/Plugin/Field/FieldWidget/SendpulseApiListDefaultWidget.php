<?php

namespace Drupal\sendpulse_api\Plugin\Field\FieldWidget;

use Drupal\Core\Field\Plugin\Field\FieldWidget\OptionsSelectWidget;

/**
 * Plugin implementation of the 'sendpulse_api_lists_default' widget.
 *
 * @FieldWidget(
 *   id = "sendpulse_api_lists_default",
 *   label = @Translation("Select list"),
 *   field_types = {
 *     "sendpulse_api_lists"
 *   },
 *   multiple_values = TRUE
 * )
 */
class SendpulseApiListDefaultWidget extends OptionsSelectWidget {
}
