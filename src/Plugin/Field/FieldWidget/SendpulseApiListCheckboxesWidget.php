<?php

namespace Drupal\sendpulse_api\Plugin\Field\FieldWidget;

use Drupal\Core\Field\Plugin\Field\FieldWidget\OptionsButtonsWidget;

/**
 * Plugin implementation of the 'sendpulse_api_lists_default' widget.
 *
 * @FieldWidget(
 *   id = "sendpulse_api_lists_checkbox",
 *   label = @Translation("Check boxes/radio buttons"),
 *   field_types = {
 *     "sendpulse_api_lists"
 *   },
 *   multiple_values = TRUE
 * )
 */
class SendpulseApiListCheckboxesWidget extends OptionsButtonsWidget {
}
