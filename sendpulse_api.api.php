<?php

/**
 * @file
 * Hooks provided by the Sendpulse Api module.
 */

use Drupal\webform\Plugin\WebformHandlerInterface;
use Drupal\webform\WebformSubmissionInterface;

/**
 * For example if you would like to send custom fields.
 *
 * @see https://sendpulse.com/integrations/api
 */

/**
 * Hook_sendpulse_api_contact_data_alter.
 *
 * Alters the contact data that is sent to Sendpulse Api.
 * This hook is triggered on both create and update of contacts.
 *
 * @param array $data
 *   - The data received from a sendpulse api form.
 * @param object $body
 *   - The content of the request that is sent to the Sendpulse Api API.
 *
 * @return void
 *   This function does not return any value but may alter the `$body` object.
 */
function hook_sendpulse_api_contact_data_alter(array $data, object &$body) {
  // Add custom field values
  // Set a value directly.
  $body->custom_fields[] = (object) [
  // The UUID of the custom field.
    'custom_field_id' => '00000000-0000-0000-0000-0000000000',
  // A value.
    'value' => 'Some Value',
  ];

  // Add custom field values
  // Set based on $data value.
  $body->custom_fields[] = (object) [
  // The UUID of the custom field.
    'custom_field_id' => '00000000-0000-0000-0000-0000000000',
  // A value.
    'value' => $data['company'],
  ];

}

/**
 * Alter mergevars before they are sent to Sendpulse Api.
 *
 * @param array $mergevars
 *   The current mergevars.
 * @param \Drupal\webform\WebformSubmissionInterface $submission
 *   The webform submission entity used to populate the mergevars.
 * @param \Drupal\webform\Plugin\WebformHandlerInterface $handler
 *   The webform submission handler used to populate the mergevars.
 *
 * @ingroup webform_sendpulse_api
 */
function hook_sendpulse_api_lists_mergevars_alter(&$mergevars, WebformSubmissionInterface $submission, WebformHandlerInterface $handler) {

}
